object Day5 : Day {

    data class Dock(val stacks : MutableList<MutableList<Char>>)

    data class Instruction(val number:Int, val source:Int, val destination: Int)

    override fun solution1(input: List<String>): String {
        val dock = Dock(mutableListOf(mutableListOf()))
        val instructions = mutableListOf<Instruction>()
        makeDockAndInstructions(input, instructions, dock)
        instructions.forEach{ moveCratesWith9000(it, dock) }
        val topLetterOnStack = dock.stacks.map { it.last() }
        return topLetterOnStack.joinToString("")
    }

    override fun solution2(input: List<String>): String {
        val dock = Dock(mutableListOf(mutableListOf()))
        val instructions = mutableListOf<Instruction>()
        makeDockAndInstructions(input, instructions, dock)
        instructions.forEach{ moveCratesWith9001(it, dock) }
        val topLetterOnStack = dock.stacks.map { it.last() }
        return topLetterOnStack.joinToString("")
    }

    private fun moveCratesWith9001(instruction: Instruction, dock: Dock){
        dock.stacks[instruction.destination].addAll(dock.stacks[instruction.source].takeLast(instruction.number))
        removeLastX(dock.stacks[instruction.source], instruction.number)
    }

    private fun removeLastX(list: MutableList<Char>, x: Int){
        when(x){
            0 -> return
            else -> {
                list.removeLast()
                removeLastX(list, x-1 )
            }
        }
    }

    private fun moveCratesWith9000(instruction: Instruction, dock: Dock){
        when (instruction.number){
            0 -> return
            else -> {
                dock.stacks[instruction.destination].add(dock.stacks[instruction.source].removeLast())
                moveCratesWith9000(Instruction(
                    instruction.number-1,
                    instruction.source,
                    instruction.destination), dock)
            }
        }
    }

    private fun makeDockAndInstructions(
        input: List<String>,
        instructions: MutableList<Instruction>,
        dock: Dock
    ) {
        input.forEach {
            if (it.contains("move")) {
                makeInstruction(it, instructions)
            }
            if (it.contains('[')) {
                reserveDockSpace(dock, it)
                loadDock(it, dock)
            }
        }
    }

    private fun loadDock(it: String, dock: Dock) {
        it.withIndex().groupBy { it.index / 4 }
            .map {
                it.value[1].value
            }
            .withIndex().forEach {
                if (it.value != ' ')
                    dock.stacks[it.index].add(0, it.value)
            }
    }

    private fun reserveDockSpace(dock: Dock, it: String) {
        while (dock.stacks.size < (it.length / 4) + 1) {
            dock.stacks.add(mutableListOf())
        }
    }

    private fun makeInstruction(it: String, instructions: MutableList<Instruction>) {
        val parts = it.split(' ')
        instructions.add(Instruction(parts[1].toInt(), parts[3].toInt()-1, parts[5].toInt()-1))
    }
}