object Day2 : Day {

    enum class Move{
        ROCK,
        PAPER,
        SCISSORS
    }

    override fun solution1(input: List<String>): String {
        val yourScore = input.map { parseLine(it) }
            .sumOf { (opp, you) -> scoreRound(transformOpponentMove(opp), transformYourMove(you)) }
        return yourScore.toString()
    }

    private fun parseLine(line: String) = Pair(line.split(' ')[0], line.split(' ')[1])

    override fun solution2(input: List<String>): String {
        val yourScore = input.map { parseLine(it) }.sumOf { (opp, you) -> scoreRound(transformOpponentMove(opp), chooseYourMoveBasedOnStrategy(you, transformOpponentMove(opp))) }
        return yourScore.toString()
    }

    private fun transformOpponentMove(move: String): Move{
        return when(move){
            "A" -> Move.ROCK
            "B" -> Move.PAPER
            "C" -> Move.SCISSORS
            else -> throw IllegalArgumentException()
        }
    }

    private fun transformYourMove(move: String): Move{
        return when(move){
            "X" -> Move.ROCK
            "Y" -> Move.PAPER
            "Z" -> Move.SCISSORS
            else -> throw IllegalArgumentException()
        }
    }

    private fun chooseYourMoveBasedOnStrategy(targetMatchOutcome: String, opponentsMove: Move): Move{
        return when(targetMatchOutcome){
            "X" -> when(opponentsMove){ //Lose
                Move.ROCK -> Move.SCISSORS
                Move.PAPER -> Move.ROCK
                Move.SCISSORS -> Move.PAPER
            }
            "Y" -> when(opponentsMove){ //Draw
                Move.ROCK -> Move.ROCK
                Move.PAPER -> Move.PAPER
                Move.SCISSORS -> Move.SCISSORS
            }
            "Z" -> when(opponentsMove){ //Win
                Move.ROCK -> Move.PAPER
                Move.PAPER -> Move.SCISSORS
                Move.SCISSORS -> Move.ROCK
            }
            else -> throw IllegalArgumentException()
        }
    }

    fun scoreRound(opponentsMove: Move, yourMove: Move): Int {
        return winningPoints(opponentsMove, yourMove) + movePoints(yourMove)
    }

    private fun movePoints(yourMove: Move) = when (yourMove) {
        Move.ROCK -> 1
        Move.PAPER -> 2
        Move.SCISSORS -> 3
    }

    private fun winningPoints(opponentsMove: Move, yourMove: Move) : Int {
        return when(opponentsMove){
            Move.ROCK -> when(yourMove){
                Move.ROCK -> 3
                Move.PAPER -> 6
                Move.SCISSORS -> 0
            }
            Move.PAPER -> when(yourMove){
                Move.ROCK -> 0
                Move.PAPER -> 3
                Move.SCISSORS -> 6
            }
            Move.SCISSORS -> when(yourMove){
                Move.ROCK -> 6
                Move.PAPER -> 0
                Move.SCISSORS -> 3
            }
        }
    }
}