object Day3 : Day {

    data class Backpack(val leftCompartment: Set<Char>, val rightCompartment: Set<Char>)

    fun makeBackpack(backpack: String): Backpack{
        return Backpack(
            backpack.take(backpack.length/2).toSet(),
            backpack.takeLast(backpack.length/2).toSet())
    }

    fun itemToValue(item: Char): Int{
        return if (item.isUpperCase()){
            item.code - 'A'.code + 27 //27 -> 52
        } else {
            item.code - 'a'.code + 1 // 1 -> 26
        }
    }

    override fun solution1(input: List<String>): String {
        val backpacks = input.map { makeBackpack(it) }
        val overlappingItems = backpacks.map { it.leftCompartment.intersect(it.rightCompartment).first() }
        val totalScore = overlappingItems.sumOf { itemToValue(it) }
        return totalScore.toString()
    }

    override fun solution2(input: List<String>): String {
        val backpacks = input.map { makeBackpack(it) }
        val groups = mutableListOf<MutableList<Backpack>>()
        backpacks.forEachIndexed { index, backpack ->
            run {
                if (index % 3 == 0)
                    groups.add(mutableListOf())
                groups.last().add(backpack)
            }
        }
        val totalScore = groups.sumOf { itemToValue(findGroupBadge(it)) }
        return totalScore.toString()
    }

    private fun findGroupBadge(backpacks: MutableList<Day3.Backpack>): Char {
        return backpacks.map { it.leftCompartment.union(it.rightCompartment) }
            .reduce(){ acc, x -> acc.intersect(x) }.first()
    }
}