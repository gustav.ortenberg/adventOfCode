import kotlin.math.min

object Day7 : Day {

    interface Node {
        fun size() : Int
        val name : String
    }

    data class Directory(override val name : String, val children : MutableList<Node>, val parent: Directory?) : Node {
        override fun size() : Int {
            return children.sumOf { it.size() }
        }
    }

    data class File(override val name: String, val size: Int) : Node {
        override fun size(): Int {
            return size
        }
    }

    override fun solution1(input: List<String>): String {
        val rootDirectory = buildFileSystem(input)
        val directories = mutableListOf<Directory>()
        findDirectoriesWithSizeLessThan(rootDirectory, 100000, directories)
        return directories.sumOf { it.size() }.toString()
    }

    override fun solution2(input: List<String>): String {
        val rootDir = buildFileSystem(input)
        val neededSpace = 30000000
        val totalSpace = 70000000
        val totalSpaceLeft = totalSpace - rootDir.size()
        val minimumSpaceNeededToBeFreed = neededSpace - totalSpaceLeft
        val bestMatchingDirectorySize = sizeOfBestMatchingDirectory(rootDir, rootDir.size(), minimumSpaceNeededToBeFreed)
        return bestMatchingDirectorySize.toString()
    }

    private fun sizeOfBestMatchingDirectory(currentDir: Directory, bestMatchingDirectorySize: Int, minimumSpaceNeededToBeFreed: Int): Int {

        if (currentDir.children.filterIsInstance<Directory>().isEmpty()){
            return if (currentDir.size() > minimumSpaceNeededToBeFreed) currentDir.size() else 70000000
        }

        return min(
            currentDir.children
            .filterIsInstance<Directory>()
            .minOf { sizeOfBestMatchingDirectory(it, bestMatchingDirectorySize, minimumSpaceNeededToBeFreed) },
            if (currentDir.size() > minimumSpaceNeededToBeFreed) currentDir.size() else 70000000)
    }


    private fun findDirectoriesWithSizeLessThan(directory: Directory, maxSize: Int, directories: MutableList<Directory>){
        directory.children.filterIsInstance<Directory>()
            .forEach { findDirectoriesWithSizeLessThan(it, maxSize, directories) }

        if (directory.size() < maxSize){
            directories.add(directory)
        }
    }

    private fun buildFileSystem(input: List<String>): Directory {
        val rootNode = Directory("/", mutableListOf(), null)
        var currentDirectory = rootNode
        input.forEach {
            if (it.startsWith("$ cd /")) {
                currentDirectory = rootNode
            } else if (it.startsWith("$ cd ..")) {
                currentDirectory = currentDirectory.parent!!
            } else if (it.startsWith("$ cd")) {
                val dirName = it.removePrefix("$ cd ")
                currentDirectory = currentDirectory.children.first { it.name == dirName } as Directory
            } else if (it.startsWith("$ ls")) {

            } else if (it.startsWith("dir ")) {
                val dirName = it.removePrefix("dir ")
                val dir = Directory(dirName, mutableListOf(), currentDirectory)
                currentDirectory.children.add(dir)
            } else {
                val file = File(it.split(' ')[1], it.split(' ')[0].toInt())
                currentDirectory.children.add(file)
            }
        }
        return rootNode
    }
}