object Day8 : Day {
    data class Tree(val height: Int, var hidden: Boolean, var scenicScore: Int){
        override fun toString(): String {
            return if (hidden) "x" else "o"
        }
    }

    override fun solution1(input: List<String>): String {
        val treeMap = mutableListOf(mutableListOf<Tree>())
        input.forEach {
            it.forEach { treeMap.last().add(Tree(it.digitToInt(), true, 0)) }
            treeMap.add(mutableListOf())
        }
        treeMap.removeLast()
        treeMap.withIndex()
            .forEach { x -> x.value.withIndex()
                .forEach{ y ->
                    if (!onTheBorder(x, treeMap, y)){
                        if (isTreeVisibleFromTheLeft(x, y)
                            || isTreeVisibleFromTheRight(x, y)
                            || isTreeVisibleFromTheTop(x.index, y.index, treeMap)
                            || isTreeVisibleFromTheBot(x.index, y.index, treeMap)){
                            y.value.hidden = false
                        }
                    }
                    else{
                        y.value.hidden = false
                    }
                } }
        return treeMap.sumOf { it.sumOf { if(it.hidden) 0 as Int else 1 } }.toString()
    }

    private fun isTreeVisibleFromTheLeft(
        x: IndexedValue<MutableList<Tree>>,
        y: IndexedValue<Tree>
    ) = x.value.subList(0, y.index).all { it.height < y.value.height }

    private fun isTreeVisibleFromTheRight(
        x: IndexedValue<MutableList<Tree>>,
        y: IndexedValue<Tree>
    ) : Boolean {
        val subList = x.value.subList(y.index+1, x.value.size)
        return subList.all { it.height < y.value.height }
    }

    private fun isTreeVisibleFromTheBot(yPos: Int, xPos: Int, treeMap: MutableList<MutableList<Tree>>): Boolean{
        for (i in yPos+1 until treeMap.size){
            if (treeMap[i][xPos].height >= treeMap[yPos][xPos].height)
                return false
        }
        return true
    }

    private fun isTreeVisibleFromTheTop(yPos: Int, xPos: Int, treeMap: MutableList<MutableList<Tree>>): Boolean{
        for (i in 0 until yPos){
            if (treeMap[i][xPos].height >= treeMap[yPos][xPos].height)
                return false
        }
        return true
    }

    private fun onTheBorder(
        x: IndexedValue<MutableList<Tree>>,
        treeMap: MutableList<MutableList<Tree>>,
        y: IndexedValue<Tree>
    ) = (x.index == 0
            || x.index == treeMap.first().size - 1
            || y.index == 0
            || y.index == treeMap.size - 1)

    override fun solution2(input: List<String>): String {
        val treeMap = mutableListOf(mutableListOf<Tree>())
        input.forEach {
            it.forEach { treeMap.last().add(Tree(it.digitToInt(), true, 1)) }
            treeMap.add(mutableListOf())
        }
        treeMap.removeLast()

        treeMap.withIndex()
            .forEach { y -> y.value.withIndex()
                .forEach{ x ->
                    if (!onTheBorder(y, treeMap, x)){
                        x.value.scenicScore *=
                            treesVisibleToLeft(treeMap, x.index, y.index) *
                                    treesVisibleToRight(treeMap, x.index, y.index) *
                                    treesVisibleToUp(treeMap, x.index, y.index) *
                                    treesVisibleToDown(treeMap, x.index, y.index)
                    }
                    else{
                        x.value.scenicScore = 0
                    }
                } }

        return treeMap.maxOf { it.maxOf { it.scenicScore } }.toString()
    }

    private fun treesVisibleToLeft(treeMap: MutableList<MutableList<Tree>>, xPos: Int, yPos: Int):Int{
        var noVisibleTrees = 0
        val range = 0 until xPos
        for (i in range.reversed()){
            noVisibleTrees++
            if (treeMap[yPos][xPos].height <= treeMap[yPos][i].height){
                return noVisibleTrees
            }
        }
        return noVisibleTrees
    }

    private fun treesVisibleToRight(treeMap: MutableList<MutableList<Tree>>, xPos: Int, yPos: Int):Int{
        var noVisibleTrees = 0
        val range = xPos+1 until treeMap.first().size
        for (i in range){
            noVisibleTrees++
            if (treeMap[yPos][xPos].height <= treeMap[yPos][i].height){
                return noVisibleTrees
            }
        }
        return noVisibleTrees
    }

    private fun treesVisibleToUp(treeMap: MutableList<MutableList<Tree>>, xPos: Int, yPos: Int):Int{
        var noVisibleTrees = 0
        val range = 0 until yPos
        for (i in range.reversed()){
            noVisibleTrees++
            if (treeMap[yPos][xPos].height <= treeMap[i][xPos].height){
                return noVisibleTrees
            }
        }
        return noVisibleTrees
    }

    private fun treesVisibleToDown(treeMap: MutableList<MutableList<Tree>>, xPos: Int, yPos: Int):Int{
        var noVisibleTrees = 0
        val range = yPos+1 until treeMap.size
        for (i in range){
            noVisibleTrees++
            if (treeMap[yPos][xPos].height <= treeMap[i][xPos].height){
                return noVisibleTrees
            }
        }
        return noVisibleTrees
    }

}