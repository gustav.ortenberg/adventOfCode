object Day6 : Day {

    override fun solution1(input: List<String>): String {
        val scanLine = input.first()
        return findMessageStart(scanLine, 4)
    }

    private fun findMessageStart(scanLine: String, noUniqueCharacters:Int): String {
        val offset = noUniqueCharacters-1
        for (i in offset..scanLine.length) {
            val window = scanLine.subSequence(i - offset, i + 1)
            if (window.toSet().size == noUniqueCharacters) {
                return (i + 1).toString()
            }
        }
        return "Not Found"
    }

    override fun solution2(input: List<String>): String {
        val scanLine = input.first()
        return findMessageStart(scanLine, 14)
    }
}