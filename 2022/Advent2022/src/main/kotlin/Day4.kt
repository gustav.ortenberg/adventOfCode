object Day4 : Day {

    data class Elf (val assignment: Set<Int>)

    override fun solution1(input: List<String>): String {
        val elfPairs = input.map { makeElfPair(it) }
        val numberOfFullyContained = elfPairs.sumOf{ if (isAssignmentFullyContained(it)) 1 else 0 as Int }
        return numberOfFullyContained.toString()
    }

    private fun isAssignmentFullyContained(elfPair: Pair<Elf, Elf>): Boolean{
        return elfPair.first.assignment.containsAll(elfPair.second.assignment)
                || elfPair.second.assignment.containsAll(elfPair.first.assignment)
    }

    override fun solution2(input: List<String>): String {
        val elfPairs = input.map { makeElfPair(it) }
        val numberOfOverlapping = elfPairs.sumOf { if (isAssignmentOverlapping(it)) 1 else 0 as Int  }
        return numberOfOverlapping.toString()
    }

    private fun isAssignmentOverlapping(elfPair: Pair<Elf, Elf>): Boolean{
        return elfPair.first.assignment.intersect(elfPair.second.assignment).isNotEmpty()
    }

    private fun makeElfPair(it: String) : Pair<Elf, Elf> {
        val elvesInPair = it.split(',')
        return Pair(makeElf( elvesInPair[0]), makeElf(elvesInPair[1]))
    }

    private fun makeElf(input: String): Elf {
        val elfDefinition = input.split('-')
        return Elf(IntRange(elfDefinition[0].toInt(), elfDefinition[1].toInt()).toSet())
    }
}