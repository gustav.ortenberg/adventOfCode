object Day1 : Day {

    data class ElfBackpack(val foodCalories : MutableList<Int>)

    override fun solution1(input: List<String>): String {
        val backpacks = MakeElfBackpacks(input);
        val biggestCalorieStash = backpacks.map { it.foodCalories.sum() }.max()
        return biggestCalorieStash.toString()
    }

    override fun solution2(input: List<String>): String {
        val backpacks = MakeElfBackpacks(input);
        val top3CalorieStash = backpacks.map { it.foodCalories.sum() }.sortedDescending().take(3).sum()
        return top3CalorieStash.toString()
    }

    fun MakeElfBackpacks(lines : List<String>) : List<ElfBackpack> {
        val backpacks = mutableListOf<ElfBackpack>()
        backpacks.add(ElfBackpack(mutableListOf()))
        lines.forEach { line ->  if (line.isNotBlank()) backpacks.last().foodCalories.add(line.toInt()) else backpacks.add(
            ElfBackpack(mutableListOf())
        ) }
        return backpacks
    }
}