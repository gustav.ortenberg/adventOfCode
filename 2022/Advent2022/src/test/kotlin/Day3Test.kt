import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test
import org.junit.jupiter.params.ParameterizedTest
import org.junit.jupiter.params.provider.CsvSource

internal class Day3Test{
    @Test
    fun `solution1 should return the correct sum`(){
        val input = listOf(
            "vJrwpWtwJgWrhcsFMMfFFhFp",
            "jqHRNqRjqzjGDLGLrsFMfFZSrLrFZsSL",
            "PmmdzqPrVvPwwTWBwg",
            "wMqvLMZHhHMvwLHjbvcjnnSBnvTQFn",
            "ttgJtRGJQctTZtZT",
            "CrZsJsPPZsGzwwsLwLmpwMDw")

        val result = Day3.solution1(input)
        assertEquals(result, "157")
    }

    @ParameterizedTest
    @CsvSource(
        "vJrwpWtwJgWrhcsFMMfFFhFp, 16",
        "jqHRNqRjqzjGDLGLrsFMfFZSrLrFZsSL, 38",
        "PmmdzqPrVvPwwTWBwg, 42",
        "wMqvLMZHhHMvwLHjbvcjnnSBnvTQFn, 22",
        "ttgJtRGJQctTZtZT, 20",
        "CrZsJsPPZsGzwwsLwLmpwMDw, 19"
    )
    fun `solution1 should return the value for a backpack`(row: String, expected: String){
        val input = listOf(row)

        val result = Day3.solution1(input)
        assertEquals(result, expected)
    }
}
