import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Test

class Day8Test {
    @Test
    fun `day8 sol1 test`(){
        val input = listOf("30373",
                "25512",
                "65332",
                "33549",
                "35390")
        val result = Day8.solution1(input)
        Assertions.assertEquals("21", result)
    }

    @Test
    fun `day8 sol2 test`(){
        val input = listOf("30373",
            "25512",
            "65332",
            "33549",
            "35390")
        val result = Day8.solution2(input)
        Assertions.assertEquals("8", result)
    }
}