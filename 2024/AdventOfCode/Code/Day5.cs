﻿namespace Code;

public class Day5
{
    /// <summary>
    /// Find the sum of the center element of each update that is correct
    /// </summary>
    public static int Part1(List<Tuple<int, int>> rules, List<List<int>> updates)
    {
        var (toPrint, _) = SplitUpdatesByOrderCorrectness(rules, updates);

        return toPrint.Select(x => x[(x.Count - 1) / 2]).Sum();
    }

    private static (List<List<int>> Correct, List<List<int>> Wrong) SplitUpdatesByOrderCorrectness(List<Tuple<int, int>> rules, List<List<int>> updates)
    {
        var updatesInCorrectOrder = new List<List<int>>();
        var updatesInWrongOrder = new List<List<int>>();
        foreach (var update in updates)
        {
            var isCorrect = true;
            for (var i = 1; i < update.Count; i++)
            {
                var before = update[..i];
                isCorrect &= !rules.Where(x => x.Item1 == update[i])
                    .Any(y => before.Contains(y.Item2));
            }

            if (isCorrect)
                updatesInCorrectOrder.Add(update);
            else
                updatesInWrongOrder.Add(update);
        }

        return (updatesInCorrectOrder, updatesInWrongOrder);
    }

    public static (List<Tuple<int, int>>, List<List<int>>) Parse(string input)
    {
        var chunks = input.Split(Environment.NewLine + Environment.NewLine);
        var rules = chunks[0]
            .Split(Environment.NewLine)
            .Select(line => line.Split('|'))
            .Select(parts => (int.Parse(parts[0]), int.Parse(parts[1])))
            .Select(pair => pair.ToTuple())
            .ToList();
        
        var updates = chunks[1]
            .Split(Environment.NewLine)
            .Select(line => line.Split(',').Select(int.Parse).ToList())
            .ToList();
        
        return (rules, updates);
    }

    /// <summary>
    /// Find the sum of the center element of each update that is incorrect, after correcting the order
    /// </summary>
    public static int Part2(List<Tuple<int,int>> rules, List<List<int>> updates)
    {
        var (_, wrong) = SplitUpdatesByOrderCorrectness(rules, updates);

        var reordered = new List<List<int>>();
        while (wrong.Count > 0)
        {
            var toCheck = Reorder(rules, wrong);
            (var correct, wrong) = SplitUpdatesByOrderCorrectness(rules, toCheck);
            reordered.AddRange(correct);
        }

        return reordered.Select(x => x[(x.Count - 1) / 2]).Sum();
    }

    private static List<List<int>> Reorder(List<Tuple<int, int>> rules, List<List<int>> wrong)
    {
        // Reorder the updates
        foreach (var update in wrong)
        {
            foreach (var rule in rules)
            {
                var indexOfBefore = update.IndexOf(rule.Item1);
                var indexOfAfter = update.IndexOf(rule.Item2);
                if (indexOfAfter < indexOfBefore && indexOfAfter != -1 && indexOfBefore != -1)
                {
                    update.Remove(rule.Item1);
                    update.Insert(indexOfAfter, rule.Item1);
                }
            }
        }

        return wrong;
    }
}