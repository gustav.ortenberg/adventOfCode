﻿namespace Code;

public class Day4
{
    public static int Part1(WordSearch input)
    {
        string[] matches = ["XMAS", "SAMX"];
        var occurrences = 0;
        for (var y = 0; y < input.height; y++)
        {
            for (var x = 0; x < input.width; x++)
            {
                //horizontal
                if (x < input.width - 3) // enough characters
                {
                    var word = input.Letters[y][x..(x + 4)];
                    if (matches.Contains(word, StringComparer.OrdinalIgnoreCase))
                        occurrences++;
                }

                //vertical
                if (y < input.height - 3)
                {
                    var word = string.Concat(
                        input.Letters[y][x],
                        input.Letters[y + 1][x],
                        input.Letters[y + 2][x],
                        input.Letters[y + 3][x]);
                    if (matches.Contains(word, StringComparer.OrdinalIgnoreCase))
                        occurrences++;
                }

                //diagonal backslash \
                if (y < input.height - 3 && x < input.width - 3)
                {
                    var word = string.Concat(
                        input.Letters[y][x],
                        input.Letters[y + 1][x + 1],
                        input.Letters[y + 2][x + 2],
                        input.Letters[y + 3][x + 3]);
                    if (matches.Contains(word, StringComparer.OrdinalIgnoreCase))
                        occurrences++;
                }

                //diagonal forward slash /
                if (y < input.height - 3 && x > 2)
                {
                    var word = string.Concat(
                        input.Letters[y][x],
                        input.Letters[y + 1][x - 1],
                        input.Letters[y + 2][x - 2],
                        input.Letters[y + 3][x - 3]);
                    if (matches.Contains(word, StringComparer.OrdinalIgnoreCase))
                        occurrences++;
                }
            }
        }

        return occurrences;
    }

    public static WordSearch Parse(string lines)
    {
        var letters = lines.Split(Environment.NewLine);
        return new WordSearch(letters, letters.Length, letters[0].Length);
    }

    public static int Part2(WordSearch input)
    {
        string[] matches = ["MAS", "SAM"];
        var occurrences = 0;
        for (var y = 0; y < input.height; y++)
        {
            for (var x = 0; x < input.width; x++)
            {
                // Find the X (cross)
                if (x < input.width - 2 && y < input.height - 2) // enough characters
                {
                    var forwardSlash = string.Concat(
                        input.Letters[y][x + 2],
                        input.Letters[y + 1][x + 1],
                        input.Letters[y + 2][x]);
                    var backslash = string.Concat(
                        input.Letters[y][x],
                        input.Letters[y + 1][x + 1],
                        input.Letters[y + 2][x + 2]);
                    if (matches.Contains(forwardSlash, StringComparer.OrdinalIgnoreCase)
                        && matches.Contains(backslash, StringComparer.OrdinalIgnoreCase))
                        occurrences++;
                }
            }
        }

        return occurrences;
    }
}

public record WordSearch(string[] Letters, int width, int height);