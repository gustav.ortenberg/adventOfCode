﻿namespace Code;

public class Day1
{
    /// <summary>
    /// Sorts and compares two lists row for row to calculate the total distance
    /// between the two lists.
    /// </summary>
    /// <param name="left">Location Ids from first Historian Group</param>
    /// <param name="right">Location Ids from second Historian Group</param>
    /// <returns>Total row distance between the lists</returns>
    public static int Part1(List<int> left, List<int> right)
    {
        left.Sort();
        right.Sort();

        return left.Zip(right)
            .Select(x => Math.Abs(x.First - x.Second))
            .Sum();
    }

    public static (List<int> left, List<int> right) ParseInput(string input)
    {
        var lines = input.Split(Environment.NewLine);
        return lines.Aggregate((new List<int>(), new List<int>()),
            (acc, line) =>
            {
                Console.WriteLine(line);
                var parts = line.Split("   ");
                acc.Item1.Add(int.Parse(parts[0]));
                acc.Item2.Add(int.Parse(parts[1]));
                return acc;
            });
    }

    /// <summary>
    /// Finds the similarity number between the right and left list.
    /// The Similarity Number is the sum of products for all elements in the left list,
    /// and it's number of occurrences in the right list.
    /// </summary>
    public static int Part2(List<int> left, List<int> right)
    {
        var rightCounts = right
            .GroupBy(n => n)
            .ToDictionary(g => g.Key, g => g.Count());

        return left.Select(x =>
                x * rightCounts.FirstOrDefault(r => r.Key == x, new KeyValuePair<int, int>(0, 0)).Value)
            .Sum();
    }
}