﻿namespace Code;

public class Day6
{
    public static int Part1(string[] map)
    {
        var mapState = FindGuard(map);
        while (!mapState.IsGuardOffMap)
        {
            mapState = TakeStep(mapState);
        }

        return mapState.Map.Select(row => row.Count(c => c == 'X')).Sum();
    }

    private static MapState FindGuard(string[] map)
    {
        for (var y = 0; y < map.Length; y++)
        {
            for (var x = 0; x < map[0].Length; x++)
            {
                if (map[y][x] == '^' || map[y][x] == 'v' || map[y][x] == '<' || map[y][x] == '>')
                {
                    return new MapState(map, (y, x));
                }
            }
        }

        throw new Exception("No guard found");
    }

    private static MapState TakeStep(MapState mapState)
    {
        if (mapState.IsGuardBlocked)
            return RotateGuard(mapState);

        return mapState.Guard switch
        {
            '^' => MoveGuard(mapState, Direction.Up),
            'v' => MoveGuard(mapState, Direction.Down),
            '<' => MoveGuard(mapState, Direction.Left),
            '>' => MoveGuard(mapState, Direction.Right),
            _ => throw new Exception("Invalid guard")
        };
    }

    private static MapState MoveGuard(MapState mapState, Direction direction)
    {
        var newMap = mapState.Map.ToArray();
        newMap[mapState.PositionOfGuard.y] = newMap[mapState.PositionOfGuard.y].Replace(mapState.Guard, 'X');

        switch (direction)
        {
            case Direction.Left:
                if (mapState.PositionOfGuard.x != 0)
                {
                    newMap[mapState.PositionOfGuard.y] =
                        newMap[mapState.PositionOfGuard.y].Remove(mapState.PositionOfGuard.x - 1, 1);
                    newMap[mapState.PositionOfGuard.y] = newMap[mapState.PositionOfGuard.y]
                        .Insert(mapState.PositionOfGuard.x - 1, mapState.Guard.ToString());
                }

                return new MapState(newMap, (mapState.PositionOfGuard.y, mapState.PositionOfGuard.x - 1));
            case Direction.Right:
                if (mapState.PositionOfGuard.x != mapState.Map[0].Length - 1)
                {
                    newMap[mapState.PositionOfGuard.y] =
                        newMap[mapState.PositionOfGuard.y].Remove(mapState.PositionOfGuard.x + 1, 1);
                    newMap[mapState.PositionOfGuard.y] = newMap[mapState.PositionOfGuard.y]
                        .Insert(mapState.PositionOfGuard.x + 1, mapState.Guard.ToString());
                }

                return new MapState(newMap, (mapState.PositionOfGuard.y, mapState.PositionOfGuard.x + 1));
            case Direction.Up:
                if (mapState.PositionOfGuard.y != 0)
                {
                    newMap[mapState.PositionOfGuard.y - 1] =
                        newMap[mapState.PositionOfGuard.y - 1].Remove(mapState.PositionOfGuard.x, 1);
                    newMap[mapState.PositionOfGuard.y - 1] = newMap[mapState.PositionOfGuard.y - 1]
                        .Insert(mapState.PositionOfGuard.x, mapState.Guard.ToString());
                }

                return new MapState(newMap, (mapState.PositionOfGuard.y - 1, mapState.PositionOfGuard.x));
            case Direction.Down:
                if (mapState.PositionOfGuard.y != mapState.Map.Length - 1)
                {
                    newMap[mapState.PositionOfGuard.y + 1] =
                        newMap[mapState.PositionOfGuard.y + 1].Remove(mapState.PositionOfGuard.x, 1);
                    newMap[mapState.PositionOfGuard.y + 1] = newMap[mapState.PositionOfGuard.y + 1]
                        .Insert(mapState.PositionOfGuard.x, mapState.Guard.ToString());
                }

                return new MapState(newMap, (mapState.PositionOfGuard.y + 1, mapState.PositionOfGuard.x));
            default:
                throw new ArgumentOutOfRangeException(nameof(direction), direction, null);
        }
    }

    private static MapState RotateGuard(MapState mapState)
    {
        var newGuard = mapState.Guard switch
        {
            '^' => '>',
            'v' => '<',
            '<' => '^',
            '>' => 'v',
            _ => throw new Exception("Invalid guard")
        };
        var newMap = mapState.Map;
        newMap[mapState.PositionOfGuard.y] = newMap[mapState.PositionOfGuard.y].Replace(mapState.Guard, newGuard);
        return mapState with { Map = newMap };
    }

    public static int Part2(string[] map)
    {
        var mapState = FindGuard(map);
        var boardSize = map.Length*map[0].Length;
        var permutations = GeneratePermutations(map, mapState.PositionOfGuard.x, mapState.PositionOfGuard.y);
        var noViablePlacements = 0;
        foreach (var permutation in permutations)
        {
            var counter = 0;
            var newMapState = FindGuard(permutation);
            while (!newMapState.IsGuardOffMap)
            {
                newMapState = TakeStep(newMapState);
                counter++;
                if (counter >= boardSize)
                {
                    noViablePlacements++;
                    break;   
                }
            }
        }
        
        return noViablePlacements;
    }

    private static List<string[]> GeneratePermutations(string[] grid, int exemptX, int exemptY)
    {
        var permutations = new List<string[]>();

        for (var y = 0; y < grid.Length; y++) 
        {
            for (var x = 0; x < grid[y].Length; x++) 
            {
                if (x == exemptX && y == exemptY || grid[y][x] == '#')
                    continue; // Skip the exempt coordinate and positions with a wall already

                var newGrid = (string[])grid.Clone(); // Clone the grid
                var row = new char[newGrid[y].Length];
                Array.Copy(newGrid[y].ToCharArray(), row, newGrid[y].Length);

                row[x] = '#'; // Replace the character at (x, y) with "#"
                newGrid[y] = new string(row);

                permutations.Add(newGrid);
            }
        }

        return permutations;
    }
}

public record MapState(string[] Map, (int y, int x) PositionOfGuard)
{
    public char Guard => Map[PositionOfGuard.y][PositionOfGuard.x];

    private char InFrontOfGuard => Guard switch
    {
        '^' => Map[PositionOfGuard.y - 1][PositionOfGuard.x],
        'v' => Map[PositionOfGuard.y + 1][PositionOfGuard.x],
        '<' => Map[PositionOfGuard.y][PositionOfGuard.x - 1],
        '>' => Map[PositionOfGuard.y][PositionOfGuard.x + 1],
        _ => throw new Exception("Invalid guard")
    };

    private bool IsGuardAtEdge => PositionOfGuard.y == 0 || PositionOfGuard.y == Map.Length - 1 ||
                                  PositionOfGuard.x == 0 || PositionOfGuard.x == Map[0].Length - 1;

    public bool IsGuardBlocked => !IsGuardAtEdge && InFrontOfGuard == '#';

    public bool IsGuardOffMap => PositionOfGuard.y < 0 || PositionOfGuard.y >= Map.Length ||
                                 PositionOfGuard.x < 0 || PositionOfGuard.x >= Map[0].Length;
}

public enum Direction
{
    Left,
    Right,
    Up,
    Down
}