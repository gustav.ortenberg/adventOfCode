﻿using System.Text.RegularExpressions;

namespace Code;

/// <summary>
/// The overarching pattern for this day is:
/// 1. Use Regex to parse all valid instructions for the task (different for Part1 and Part2)
/// 2. Identify valid mul instructions
/// 3. Apply mul instructions and then sum up the products
/// </summary>
public class Day3
{
    private const string _instructionsPattern = $@"({_mulPattern})|(do\(\))|(don't\(\))";
    private const string _mulPattern = @"mul\(\d{1,3},\d{1,3}\)";

    public static int Part1(string input)
    {
        var mulInstructions = Regex.Matches(input, _mulPattern).Select(match => match.Value);
        return MulThenSumUp(mulInstructions);
    }

    private static int MulThenSumUp(IEnumerable<string> mulInstructions)
    {
        var products = mulInstructions.Select(match => Regex.Matches(match, @"\d{1,3}"))
            .Select(x => int.Parse(x[0].Value) * int.Parse(x[1].Value));
        return products.Sum();
    }

    public static int Part2(string input)
    {
        var enabled = true;
        var mulInstructions = new List<string>();
        var instructions = Regex.Matches(input, _instructionsPattern);
        for (var i = 0; i < instructions.Count; i++)
        {
            var instruction = instructions[i].Value;
            switch (instruction)
            {
                case "don't()":
                    enabled = false;
                    break;
                case "do()":
                    enabled = true;
                    break;
                default:
                    if (enabled) mulInstructions.Add(instruction);
                    break;
            }
        }

        return MulThenSumUp(mulInstructions);
    }
}