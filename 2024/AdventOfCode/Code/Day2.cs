﻿namespace Code;

public class Day2
{
    public static List<Report> ParseInput(string input)
    {
        return input
            .Split(Environment.NewLine)
            .Select(line =>
                line.Split(" ")
                    .Select(int.Parse)
                    .ToList())
            .Select(levels => new Report(levels))
            .ToList();
    }

    public static int Part1(List<Report> reports)
    {
        return reports.Count(report => report.IsSafe(report.Levels));
    }

    public static int Part2(List<Report> reports)
    {
        return reports.Count(report => report.IsDampenerSafe());
    }
}

public record Report(List<int> Levels)
{
    public bool IsSafe(List<int> list) => IsStrictlyMonotonic(list) && IsMax3Apart(list);

    private static bool IsMax3Apart(List<int> list) => list.Zip(list.Skip(1), (a, b) => Math.Abs(a-b) <= 3).All(x => x);

    /// <summary>
    /// IsStrictly___ takes care of the criteria differing by at least 1
    /// Monotonic takes care of the criteria of only Asc or Desc
    /// </summary>
    private bool IsStrictlyMonotonic(List<int> list) => IsStrictlyAscending(list) || IsStrictlyDescending(list);
    private bool IsStrictlyAscending(List<int> list) => list.Zip(list.Skip(1), (a, b) => a < b).All(x => x);
    private bool IsStrictlyDescending(List<int> list) => list.Zip(list.Skip(1), (a, b) => a > b).All(x => x);

    /// <summary>
    /// Dampener safe means that it IsSafe allowing for 1 level to be removed from the sequence
    /// </summary>
    public bool IsDampenerSafe() => IsSafe(Levels) || GetPermutationsWithOneMissing(Levels).Exists(IsSafe);
    
    private static List<List<int>> GetPermutationsWithOneMissing(List<int> list)
    {
        var result = new List<List<int>>();

        for (var i = 0; i < list.Count; i++)
        {
            var perm = new List<int>(list);
            perm.RemoveAt(i);
            result.Add(perm);
        }

        return result;
    }
}