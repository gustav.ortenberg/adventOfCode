﻿using Code;

namespace Tests;

public class Day6Tests
{
    private string _input;

    [SetUp]
    public void Setup()
    {
        _input = "....#.....\r\n.........#\r\n..........\r\n..#.......\r\n.......#..\r\n..........\r\n.#..^.....\r\n........#.\r\n#.........\r\n......#...";
    }
    [Test]
    public void Part1_HappyCase()
    {
        var result = Day6.Part1(_input.Split(Environment.NewLine));

        result.Should().Be(41);
    }
    
    [Test]
    public void Part1_Run()
    {
        var lines = File.ReadAllText("C:/extra/adventOfCode/2024/AdventOfCode/Code/Inputs/Day6");
        var result = Day6.Part1(lines.Split(Environment.NewLine));

        result.Should().Be(5162);
    }
    
    [Test]
    public void Part2_HappyCase()
    {
        var result = Day6.Part2(_input.Split(Environment.NewLine));

        result.Should().Be(6);
    }
    
    [Test]
    public void Part2_Run()
    {
        var lines = File.ReadAllText("C:/extra/adventOfCode/2024/AdventOfCode/Code/Inputs/Day6");
        var result = Day6.Part2(lines.Split(Environment.NewLine));

        result.Should().Be(1909);
    }
}