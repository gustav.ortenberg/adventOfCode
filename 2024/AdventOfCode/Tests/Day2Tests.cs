﻿using Code;
using FluentAssertions;

namespace Tests;

public class Day2Tests
{
    private List<Report> _sampleReports;

    [SetUp]
    public void Setup()
    {
        _sampleReports =
        [
            new Report([7, 6, 4, 2, 1]),
            new Report([1, 2, 7, 8, 9]),
            new Report([9, 7, 6, 2, 1]),
            new Report([1, 3, 2, 4, 5]),
            new Report([8, 6, 4, 4, 1]),
            new Report([1, 3, 6, 7, 9])
        ];
    }

    [Test]
    public void Part1_HappyCase()
    {
        var result = Day2.Part1(_sampleReports);

        result.Should().Be(2);
    }

    [Test]
    public void ParseInput_HappyCase()
    {
        const string input = "7 6 4 2 1\r\n1 2 7 8 9\r\n9 7 6 2 1\r\n1 3 2 4 5\r\n8 6 4 4 1\r\n1 3 6 7 9";

        var result = Day2.ParseInput(input);
        
        result.Should().BeEquivalentTo(_sampleReports);
    }

    [Test]
    public void Run_Part1()
    {
        var input = File.ReadAllText("C:/extra/adventOfCode/2024/AdventOfCode/Code/Inputs/Day2");
        var reports = Day2.ParseInput(input);
        var result = Day2.Part1(reports);
        result.Should().Be(534);
    }

    [Test]
    public void Part2_HappyCase()
    {
        var result = Day2.Part2(_sampleReports);

        result.Should().Be(4);
    }

    [Test]
    public void Run_Part2()
    {
        var input = File.ReadAllText("C:/extra/adventOfCode/2024/AdventOfCode/Code/Inputs/Day2");
        var reports = Day2.ParseInput(input);
        var result = Day2.Part2(reports);
        result.Should().Be(577);
    }
}