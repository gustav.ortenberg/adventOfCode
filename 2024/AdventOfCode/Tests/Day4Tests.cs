﻿using Code;

namespace Tests;

[TestFixture]
public class Day4Tests
{
    private WordSearch input;
    
    [SetUp]
    public void Setup()
    {
        var str = "MMMSXXMASM\r\nMSAMXMSMSA\r\nAMXSXMAAMM\r\nMSAMASMSMX\r\nXMASAMXAMM\r\nXXAMMXXAMA\r\nSMSMSASXSS\r\nSAXAMASAAA\r\nMAMMMXMMMM\r\nMXMXAXMASX";
        input = new WordSearch(str.Split(Environment.NewLine), 10, 10);
    }
    
    [Test]
    public void Part1_HappyCase()
    {
        var result = Day4.Part1(input);

        result.Should().Be(18);
    }

    [Test]
    public void Part1_Run()
    {
        var lines = File.ReadAllText("C:/extra/adventOfCode/2024/AdventOfCode/Code/Inputs/Day4");
        var wordSearch = Day4.Parse(lines);

        var result = Day4.Part1(wordSearch);

        result.Should().Be(2685);
    }

    [Test]
    public void Part2_HappyCase()
    {
        var result = Day4.Part2(input);

        result.Should().Be(9);
    }

    [Test]
    public void Part2_Run()
    {
        var lines = File.ReadAllText("C:/extra/adventOfCode/2024/AdventOfCode/Code/Inputs/Day4");
        var wordSearch = Day4.Parse(lines);

        var result = Day4.Part2(wordSearch);

        result.Should().Be(2048);
    }
}