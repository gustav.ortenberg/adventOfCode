﻿using Code;

namespace Tests;

public class Day1Tests
{
    private List<int> Left { get; set; }
    private List<int> Right { get; set; }

    [SetUp]
    public void Setup()
    {
        Left = [3, 4, 2, 1, 3, 3];
        Right = [4, 3, 5, 3, 9, 3];
    }

    [Test]
    public void TestPart1_HappyCase()
    {
        var result = Day1.Part1(Left, Right);

        result.Should().Be(11);
    }

    [Test]
    public void TestParseInput_HappyCase_WindowsLineEnding()
    {
        var input = "3   4\r\n4   3\r\n2   5\r\n1   3\r\n3   9\r\n3   3";

        var result = Day1.ParseInput(input);

        result.left.Should().BeEquivalentTo(Left);
        result.right.Should().BeEquivalentTo(Right);
    }

    [Test]
    public void Run_Part1()
    {
        var input = File.ReadAllText("C:/extra/adventOfCode/2024/AdventOfCode/Code/Inputs/Day1");
        var (left, right) = Day1.ParseInput(input);
        var result = Day1.Part1(left, right);
        result.Should().Be(1580061);
    }

    [Test]
    public void Part2_HappyCase()
    {
        var result = Day1.Part2(Left, Right);

        result.Should().Be(31);
    }

    [Test]
    public void Run_Part2()
    {
        var input = File.ReadAllText("C:/extra/adventOfCode/2024/AdventOfCode/Code/Inputs/Day1");
        var (left, right) = Day1.ParseInput(input);
        var result = Day1.Part2(left, right);
        result.Should().Be(23046913);
    }
}