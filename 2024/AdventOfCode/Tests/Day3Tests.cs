﻿using Code;

namespace Tests;

public class Day3Tests
{
    [Test]
    public void TestPart1_HappyCase()
    {
        const string input = "xmul(2,4)%&mul[3,7]!@^do_not_mul(5,5)+mul(32,64]then(mul(11,8)mul(8,5))";

        var result = Day3.Part1(input);

        result.Should().Be(161);
    }

    [Test]
    public void Run_Part1()
    {
        var input = File.ReadAllText("C:/extra/adventOfCode/2024/AdventOfCode/Code/Inputs/Day3");
        var result = Day3.Part1(input);
        result.Should().Be(174336360);
    }

    [Test]
    public void TestPart2_HappyCase()
    {
        const string input = "xmul(2,4)&mul[3,7]!^don't()_mul(5,5)+mul(32,64](mul(11,8)undo()?mul(8,5))";

        var result = Day3.Part2(input);

        result.Should().Be(48);
    }

    [Test]
    public void Run_Part2()
    {
        var input = File.ReadAllText("C:/extra/adventOfCode/2024/AdventOfCode/Code/Inputs/Day3");
        var result = Day3.Part2(input);
        result.Should().Be(88802350);
    }

    [Test]
    public void Part2_Should_be_smaller_than_Part1()
    {
        var input = File.ReadAllText("C:/extra/adventOfCode/2024/AdventOfCode/Code/Inputs/Day3");
        var part1 = Day3.Part1(input);
        var part2 = Day3.Part2(input);
        part2.Should().BeLessThan(part1);
    }
}