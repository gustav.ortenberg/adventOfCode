﻿using Code;

namespace Tests;

public class Day5Tests
{
    private List<Tuple<int, int>> _rules;
    private List<List<int>> _updates;

    [SetUp]
    public void Setup()
    {
        _rules =
        [
            new Tuple<int, int>(47, 53),
            new Tuple<int, int>(97, 13),
            new Tuple<int, int>(97, 61),
            new Tuple<int, int>(97, 47),
            new Tuple<int, int>(75, 29),
            new Tuple<int, int>(61, 13),
            new Tuple<int, int>(75, 53),
            new Tuple<int, int>(29, 13),
            new Tuple<int, int>(97, 29),
            new Tuple<int, int>(53, 29),
            new Tuple<int, int>(61, 53),
            new Tuple<int, int>(97, 53),
            new Tuple<int, int>(61, 29),
            new Tuple<int, int>(47, 13),
            new Tuple<int, int>(75, 47),
            new Tuple<int, int>(97, 75),
            new Tuple<int, int>(47, 61),
            new Tuple<int, int>(75, 61),
            new Tuple<int, int>(47, 29),
            new Tuple<int, int>(75, 13),
            new Tuple<int, int>(53, 13)
        ];
        _updates =
        [
            new List<int> { 75, 47, 61, 53, 29 },
            new List<int> { 97, 61, 53, 29, 13 },
            new List<int> { 75, 29, 13 },
            new List<int> { 75, 97, 47, 61, 53 },
            new List<int> { 61, 13, 29 },
            new List<int> { 97, 13, 75, 29, 47 }
        ];
    }

    [Test]
    public void Part1_HappyCase()
    {
        var result = Day5.Part1(_rules, _updates);

        result.Should().Be(143);
    }

    [Test]
    public void Part1_Run()
    {
        var lines = File.ReadAllText("C:/extra/adventOfCode/2024/AdventOfCode/Code/Inputs/Day5");
        var (rules, updates) = Day5.Parse(lines);

        var result = Day5.Part1(rules, updates);

        result.Should().Be(6384);
    }

    [Test]
    public void Part2_HappyCase()
    {
        var result = Day5.Part2(_rules, _updates);

        result.Should().Be(123);
    }

    [Test]
    public void Part2_Run()
    {
        var lines = File.ReadAllText("C:/extra/adventOfCode/2024/AdventOfCode/Code/Inputs/Day5");
        var (rules, updates) = Day5.Parse(lines);

        var result = Day5.Part2(rules, updates);

        result.Should().Be(5353);
    }
}