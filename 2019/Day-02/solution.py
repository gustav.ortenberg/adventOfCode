import sys
#Fix import of int PC

def readAndFormat(inputFile):
	opCodes = []
	with open(inputFile) as file:
		opCodes = map(int, file.readline().split(','))
	return opCodes

def solvePart2(opCodes, target):
	for noun in range(0,99):
		for verb in range(0,99):
			if target == intCompute(opCodes, noun, verb):
				return (noun, verb)

def main():
	if len(sys.argv) == 2:
		inputFile = sys.argv[1]
	else:
		print('Please provide a path to an input file.')
		return

	opCodes = readAndFormat(inputFile)
	print('Day 2:')
	print('Part 1: ', intCompute(opCodes, 12, 2))
	print('Part 2: ', solvePart2(opCodes, 19690720))

if __name__ == '__main__':
    main()
