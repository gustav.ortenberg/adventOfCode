
#Read input => List of Numbers
def fileToList(fileName):
	listOfNumbers = []
	with open(fileName) as file:
		for line in file:
			numbers = list(map(int, line.split(',')))
			if len(numbers) > 1:
				listOfNumbers.append(tuple(numbers))
			else :
				listOfNumbers.append(numbers[0])    
	return listOfNumbers

def requiredFuelForModule(moduleMass):
	return moduleMass // 3 - 2

def testRequiredFuleForModule():
	testData = fileToList('test1')

	for inData, expected in testData:
		outData = requiredFuelForModule(inData)
		if expected != outData:
			print('RFFM gave {} as output with {} as input. {} was expected.'.format(outData, inData, expected))

def recursiveFule(weight):
	if weight > 6:
		extraWeight = requiredFuelForModule(weight)
		return extraWeight + recursiveFule(extraWeight)
	else:
		return 0 

def solvePart1(moduleMassList):
	requiredFuel = sum(map(requiredFuelForModule, moduleMassList))
	return requiredFuel

def solvePart2(moduleMassList):
	requiredFuel = sum(map(recursiveFule, moduleMassList))
	return requiredFuel

#Main function
def main():
    print('Day 1')
    testRequiredFuleForModule()
    moduleMassList = fileToList('input')
    ans1 = solvePart1(moduleMassList)
    print('1: Fule required: ', ans1)
    ans2 = solvePart2(moduleMassList)
    print('2: Fule required: ', ans2)


if __name__ == '__main__':
    main()