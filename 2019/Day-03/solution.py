import sys

def readAndFormat(filePath):
	theMap = dict()
	instructions = []
	with open(filePath) as file:
		wire = 'A'
		for line in file:
			pos = {'x':0,'y':0}
			w = []
			for instruction in line.split(','):
				cardinal = instruction[0]
				steps = (int)(instruction[1:])
				w.append((cardinal, steps))
				pos = mark(theMap, pos, cardinal, steps, wire)
			instructions.append(w)
			wire = 'B'
	return theMap, instructions

def mark(theMap, pos, cardinal, steps, wire):
	for i in range(0, steps):
		if cardinal == 'R':	
			pos['x'] += 1;
		elif cardinal == 'L':
			pos['x'] -= 1;
		elif cardinal == 'U':
			pos['y'] += 1;
		elif cardinal == 'D':
			pos['y'] -= 1;

		strPos = ','.join(map(str,[pos['x'],pos['y']]))
		if strPos in theMap:
			if theMap[strPos] not in [wire,'X']:
				theMap[strPos] = 'X'
		else:
			theMap[strPos] = wire
	return pos

def filterIntersections(theMap):
	intersections = []
	for k in theMap:
		if theMap[k] == 'X':
			intersections.append(map(int,k.split(',')))
	return intersections

'''
Calculates the manhattan distance between two points a and b
a   | point A  | obj {x:Int, y:Int}
b   | point B  | obj {x:Int, y:Int}
out | distance | Int
'''
def manhattanDistance(a, b):
	distance = abs(a['x'] - b['x']) + abs(a['y'] - b['y'])
	return distance

def solve1(intersections):
	origin = {'x':0,'y':0}
	shortest = 1000000000
	for x,y in intersections:
		distance = manhattanDistance({'x':x,'y':y}, origin)
		if distance < shortest:
			shortest = distance	
	return shortest

def main():
	if len(sys.argv) == 2:
		theMap, instructions = readAndFormat(sys.argv[1])
	else:
		print('Provide a input file.')
		return

	intersections = filterIntersections(theMap)
	print('Part 1: ', solve1(intersections))

if __name__ == '__main__':
	main()


'''
Do: calculate the number of steps each wire takes to reach each intersection; 
choose the intersection where the sum of both wires' steps is lowest. 

If a wire visits a position on the grid multiple times, 
use the steps value from the first time it visits that position when 
calculating the total value of a specific intersection.

The number of steps a wire takes is the total number of grid squares the wire 
has entered to get to that location, including the intersection being considered. 
...........
.+-----+...
.|.....|...
.|..+--X-+.
.|..|..|.|.
.|.-X--+.|.
.|..|....|.
.|.......|.
.o-------+.
...........
In the above example, the intersection closest to the central port is reached 
after 8+5+5+2 = 20 steps by the first wire and 7+6+4+3 = 20 steps by the second 
wire for a total of 20+20 = 40 steps.

However, the top-right intersection is better: 
the first wire takes only 8+5+2 = 15 and the second wire takes only 7+6+2 = 15, 
a total of 15+15 = 30 steps.

Here are the best steps for the extra examples from above:

R75,D30,R83,U83,L12,D49,R71,U7,L72
U62,R66,U55,R34,D71,R55,D58,R83 = 610 steps
R98,U47,R26,D63,R33,U87,L62,D20,R33,U53,R51
U98,R91,D20,R16,D67,R40,U7,R15,U6,R7 = 410 steps
'''