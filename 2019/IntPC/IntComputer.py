'''
Takes an intCode program and two initilizing values for the program
and calculates the resulting output of that program.
ogList : IntCode program              | [Int]
noun   : Initializer at position 0    | Int
verb   : Initializer at position 1    | Int
return : Program output at position 0 | Int
'''
def intCompute(ogList, noun, verb):
	halt = False
	ptr = 0
	opCodes = ogList[:]       #Init memory
	opCodes[1:3] = noun, verb #Init memory
	while not halt:
		op = opCodes[ptr]
		if op == 1:
			(arg1, arg2, dst) = opCodes[ptr+1:ptr+4]
			ptr += 4
			opCodes[dst] = opCodes[arg1] + opCodes[arg2]
		elif op == 2:
			(arg1, arg2, dst) = opCodes[ptr+1:ptr+4]
			ptr += 4
			opCodes[dst] = opCodes[arg1] * opCodes[arg2]
		elif op == 99:
			halt = True
		else:
			print('The IntComputer has encoundtered an unknown opcode.\n Please implement!')
			return
	return opCodes[0]