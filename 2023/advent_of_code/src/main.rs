mod day_one;
mod day_six;
mod day_seven;
mod day_two;
mod util;

fn main() {
    day_one::day_one_first_part();
    day_one::day_one_second_part();
    day_two::part_one();
    day_two::part_two();
    day_six::part_one();
    day_six::part_two();
    //day_seven::part_one();
    day_seven::part_two();
}
