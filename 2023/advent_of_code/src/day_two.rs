use std::cmp;

#[derive(Debug)]
#[derive(PartialEq)]
struct Show {
    red: u32,
    blue: u32,
    green: u32,
}

#[derive(Debug)]
#[derive(PartialEq)]
struct Game {
    number: u32,
    shows: Vec<Show>,
}

fn parse_line(line: &str) -> Option<Game> {
    let parts: Vec<&str> = line.split(": ").collect();

    if let Some(game_info) = parts.get(1) {
        let game_number: u32 = parts[0].trim().replace("Game", "").trim().parse().ok()?;
        let shows: Vec<Show> = game_info
            .split("; ")
            .map(|show_info| {
                let colors: Vec<&str> = show_info.split(", ").collect();
                let mut show = Show {
                    red: 0,
                    blue: 0,
                    green: 0,
                };

                for color_info in colors {
                    let parts: Vec<&str> = color_info.split_whitespace().collect();
                    if let Some(count) = parts.get(0) {
                        if let Some(color) = parts.get(1) {
                            match *color {
                                "red" => show.red = count.parse().unwrap_or(0),
                                "blue" => show.blue = count.parse().unwrap_or(0),
                                "green" => show.green = count.parse().unwrap_or(0),
                                _ => {}
                            }
                        }
                    }
                }

                show
            })
            .collect();

        Some(Game { number: game_number, shows })
    } else {
        None
    }
}

fn game_fits_rule<F>(game: &Game, rule: F) -> bool
    where F: Fn(&Show) -> bool
{
    !game.shows.iter().any(|show| rule(show))
}

fn part_one_rule(show: &Show) -> bool {
    show.blue > 14 || show.green > 13 || show.red > 12 //This is a violation to the rule of 0-X of each color
}

fn power(game: Game) -> u32{
    let mut red = 0;
    let mut blue = 0;
    let mut green = 0;

    game.shows.iter().for_each(|show| {
        red = cmp::max(show.red, red);
        blue = cmp::max(show.blue, blue);
        green = cmp::max(show.green, green);
    });
    red*blue*green
}

pub fn part_one() {
    match crate::util::read_file_to_string("C:\\extra\\adventOfCode\\2023\\advent_of_code\\src\\day_two_input") {
        Ok(content) => {
            println!("Day 2 Part 1 result: {}", part_one_composition(content))
        }
        Err(err) => { eprintln!("Error reading file: {}", err) }
    }
}

fn part_one_composition(content: String) -> u32 {
    content.lines()
        .map(|line| parse_line(line))
        .filter_map(|game| match game {
            Some(game) => Some(game),
            None => None
        })
        .filter(|game| game_fits_rule(game, part_one_rule))
        .map(|game| game.number)
        .sum::<u32>()
}

pub fn part_two() {
    match crate::util::read_file_to_string("C:\\extra\\adventOfCode\\2023\\advent_of_code\\src\\day_two_input") {
        Ok(content) => {
            println!("Day 2 Part 2 result: {}", part_two_composition(content))
        }
        Err(err) => { eprintln!("Error reading file: {}", err) }
    }
}

fn part_two_composition(content: String) -> u32 {
    content.lines()
        .map(|line| parse_line(line))
        .filter_map(|game| match game {
            Some(game) => Some(game),
            None => None
        })
        .map(|game| power(game))
        .sum::<u32>()
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_parse_line_game_1() {
        let input_line = "Game 1: 3 blue, 4 red; 1 red, 2 green, 6 blue; 2 green";
        let expected_game = Game {
            number: 1,
            shows: vec![
                Show { red: 4, blue: 3, green: 0 },
                Show { red: 1, blue: 6, green: 2 },
                Show { red: 0, blue: 0, green: 2 },
            ],
        };
        assert_eq!(parse_line(input_line), Some(expected_game));
    }

    #[test]
    fn test_parse_line_game_2() {
        let input_line = "Game 2: 1 blue, 2 green; 3 green, 4 blue, 1 red; 1 green, 1 blue";
        let expected_game = Game {
            number: 2,
            shows: vec![
                Show { red: 0, blue: 1, green: 2 },
                Show { red: 1, blue: 4, green: 3 },
                Show { red: 0, blue: 1, green: 1 },
            ],
        };
        assert_eq!(parse_line(input_line), Some(expected_game));
    }

    #[test]
    fn test_parse_line_game_3() {
        let input_line = "Game 3: 8 green, 6 blue, 20 red; 5 blue, 4 red, 13 green; 5 green, 1 red";
        let expected_game = Game {
            number: 3,
            shows: vec![
                Show { red: 20, blue: 6, green: 8 },
                Show { red: 4, blue: 5, green: 13 },
                Show { red: 1, blue: 0, green: 5 },
            ],
        };
        assert_eq!(parse_line(input_line), Some(expected_game));
    }

    #[test]
    fn test_parse_line_game_4() {
        let input_line = "Game 4: 1 green, 3 red, 6 blue; 3 green, 6 red; 3 green, 15 blue, 14 red";
        let expected_game = Game {
            number: 4,
            shows: vec![
                Show { red: 3, blue: 6, green: 1 },
                Show { red: 6, blue: 0, green: 3 },
                Show { red: 14, blue: 15, green: 3 },
            ],
        };
        assert_eq!(parse_line(input_line), Some(expected_game));
    }

    #[test]
    fn test_parse_line_game_5() {
        let input_line = "Game 5: 6 red, 1 blue, 3 green; 2 blue, 1 red, 2 green";
        let expected_game = Game {
            number: 5,
            shows: vec![
                Show { red: 6, blue: 1, green: 3 },
                Show { red: 1, blue: 2, green: 2 },
            ],
        };
        assert_eq!(parse_line(input_line), Some(expected_game));
    }

    #[test]
    fn test_power_1() {
        let game = Game {
            number: 1,
            shows: vec![
                Show { red: 4, blue: 3, green: 0 },
                Show { red: 1, blue: 6, green: 2 },
                Show { red: 0, blue: 0, green: 2 },
            ],
        };
        assert_eq!(power(game), 48);
    }

    #[test]
    fn test_power_2() {
        let game = Game {
            number: 2,
            shows: vec![
                Show { red: 0, blue: 1, green: 2 },
                Show { red: 1, blue: 4, green: 3 },
                Show { red: 0, blue: 1, green: 1 },
            ],
        };
        assert_eq!(power(game), 12);
    }

    #[test]
    fn test_power_3() {
        let game = Game {
            number: 3,
            shows: vec![
                Show { red: 20, blue: 6, green: 8 },
                Show { red: 4, blue: 5, green: 13 },
                Show { red: 1, blue: 0, green: 5 },
            ],
        };
        assert_eq!(power(game), 1560);
    }

    #[test]
    fn test_power_4() {
        let game = Game {
            number: 4,
            shows: vec![
                Show { red: 3, blue: 6, green: 1 },
                Show { red: 6, blue: 0, green: 3 },
                Show { red: 14, blue: 15, green: 3 },
            ],
        };
        assert_eq!(power(game), 630);
    }

    #[test]
    fn test_power_5() {
        let game = Game {
            number: 5,
            shows: vec![
                Show { red: 6, blue: 1, green: 3 },
                Show { red: 1, blue: 2, green: 2 },
            ],
        };
        assert_eq!(power(game), 36);
    }
}