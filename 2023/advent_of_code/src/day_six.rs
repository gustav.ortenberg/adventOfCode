struct RecordToBeat {
    race_time: u64,
    distance_to_beat: u64
}

fn number_of_ways_to_beat_record(record_to_beat: RecordToBeat) -> u64{
    let record_beats: Vec<u64> = (1..record_to_beat.race_time)
        .map(|x| (record_to_beat.race_time - x)*x) // (time traveling) * acceleration time
        .filter(|&distance_traveled| distance_traveled > record_to_beat.distance_to_beat)
        .collect();

    return record_beats.len() as u64;
}

pub fn part_one(){
    let record_1 = RecordToBeat{race_time: 60, distance_to_beat: 601};
    let record_2 = RecordToBeat{race_time: 80, distance_to_beat: 1163};
    let record_3 = RecordToBeat{race_time: 86, distance_to_beat: 1559};
    let record_4 = RecordToBeat{race_time: 76, distance_to_beat: 1300};

    let records = [record_1, record_2, record_3, record_4];

    let result: u64 = records.map(number_of_ways_to_beat_record).iter().product();
    println!("Day 6 Part 1 result: {}", result)
}

pub fn part_two(){
    println!("Day 6 Part 2 result: {}",
             number_of_ways_to_beat_record(RecordToBeat{race_time:60808676,distance_to_beat:601116315591300}))
}

#[cfg(test)]
mod tests {
    use test_case::test_case;
    use crate::day_six::{number_of_ways_to_beat_record, RecordToBeat};

    #[test_case(7, 9, 4 ; "first sample race")]
    #[test_case(15, 40, 8 ; "second sample race")]
    #[test_case(30, 200, 9 ; "third sample race")]
    #[test_case(71530, 940200, 71503 ; "part 2 sample")]
    fn test_number_of_ways_to_beat_record(time: u64, distance: u64, expected: u64){
        let record_to_beat = RecordToBeat { race_time: time, distance_to_beat: distance };
        assert_eq!(number_of_ways_to_beat_record(record_to_beat), expected)
    }
}