use std::collections::HashMap;

#[derive(PartialEq)]
#[derive(Debug)]
enum HandType {
    FiveOfAKind,
    FourOfAKind,
    FullHouse,
    ThreeOfAKind,
    TwoPair,
    OnePair,
    HighCard
}

#[derive(Eq, Hash, PartialEq, Debug)]
enum Card {
    Ace,
    King,
    Queen,
    Jack,
    Ten,
    Nine,
    Eight,
    Seven,
    Six,
    Five,
    Four,
    Three,
    Two
}

struct Hand {
    ordered: [Card; 5],
    buckets: HashMap<Card,i32>
}

struct HandWithType{
    hand: Hand,
    h_type: HandType
}

fn to_hand_type(hand: Hand) -> HandType {
    if hand.buckets.len() == 1 { return HandType::FiveOfAKind; }

   assert!(hand.buckets.len() > 0 && hand.buckets.len() <= 5);
   return match hand.buckets.len() {
        1 => HandType::FiveOfAKind,
        2 => match hand.buckets.values().next() {
            Some(1) | Some(4) => HandType::FourOfAKind,
            Some(2) | Some(3) => HandType::FullHouse,
            _ => HandType::HighCard //Actually unreachable state
        }
        3 => if hand.buckets.iter().any(|(_,&x)| x == 3)
        { HandType::ThreeOfAKind }
        else { HandType::TwoPair }
        4 => HandType::OnePair,
        5 => HandType::HighCard,
        _ => HandType::HighCard// Actually unreachable state
   }
}

fn to_card_type(p0: char) -> Card{
    match p0 {
        'A' => Card::Ace,
        'K' => Card::King,
        'Q' => Card::Queen,
        'J' => Card::Jack,
        'T' => Card::Ten,
        '9' => Card::Nine,
        '8' => Card::Eight,
        '7' => Card::Seven,
        '6' => Card::Six,
        '5' => Card::Five,
        '4' => Card::Four,
        '3' => Card::Three,
        '2' => Card::Two,
        _ => Card::Two // But should never happen
    }
}

fn to_hand(p0: &str) -> Hand {
    let char_counts: HashMap<_, _> = p0.chars().fold(HashMap::new(), |mut acc, ch| {
        *acc.entry(to_card_type(ch)).or_insert(0) += 1;
        acc
    });

    let ordered: Vec<Card> = p0.chars().map(to_card_type).collect();
    assert_eq!(ordered.len(), 5);

    Hand{ordered: <[Card; 5]>::try_from(ordered).unwrap(), buckets: char_counts}
}

fn to_hand_with_type (hand: Hand) -> HandWithType{
    //let hand_type = to_hand_type(hand);
    HandWithType{
        hand: hand,
        h_type: HandType::HighCard
    }
}

fn rank_hands() -> Vec<(u32, u32)>{
    Vec::from([(0, 0)])
}

fn part_one_composition(p0: String) -> u32{
    //Split ' '
    //Find bid
    let x = p0.split('\n')
        .map(split_words)
        .map(|(hand, bid)| (to_hand_with_type(to_hand(&hand)),to_int(bid)))
        .map(|(hand, bid)| 0);
    //Find hand rank
    let ranked = rank_hands();
    ranked.iter()
        .map(|&(rank, bid)| rank * bid)
        .sum()

}

pub fn part_one(){
    match crate::util::read_file_to_string("C:\\extra\\adventOfCode\\2023\\advent_of_code\\src\\day_seven_input") {
        Ok(content) => {
            println!("Day 7 Part 1 result: {}", content)
        }
        Err(err) => {eprintln!("Error reading file: {}", err)}
    }
}

pub fn part_two(){
    println!("Day 7 Part 2 result: {}", -1)
}

fn split_words(line: &str) -> (String, String) {
    let words: Vec<&str> = line.split_whitespace().collect();

    if let Some(first_word) = words.get(0) {
        let rest_of_first = first_word.chars().skip(1).collect::<String>();

        let second_word = words.get(1).map(|w| w.to_string()).unwrap_or_default();

        (
            format!("{}{}", first_word, rest_of_first),
            second_word,
        )
    } else {
        (String::new(), String::new())
    }
}

fn to_int(s: String) -> i32 {
    let result: Result<i32, _> = s.parse();

    match result {
        Ok(parsed_value) => { parsed_value }
        Err(_) => {0}
    }
}

#[cfg(test)]
mod tests {
    use std::collections::HashMap;
    use test_case::test_case;
    use crate::day_seven::{Card, Hand, HandType};

    #[test_case("32T3K", HandType::OnePair; "first sample")]
    #[test_case("T55J5", HandType::ThreeOfAKind; "second sample")]
    #[test_case("KK677", HandType::TwoPair; "third sample")]
    #[test_case("KTJJT", HandType::TwoPair; "fourth sample")]
    #[test_case("QQQJA", HandType::ThreeOfAKind; "fifth sample")]
    fn test_str_to_hand_type(input: &str, expected: HandType){
        let hand = crate::day_seven::to_hand(input);
        assert_eq!(crate::day_seven::to_hand_type(hand), expected)
    }

    #[test]
    fn test_to_hand_type(){
        let cards = [Card::Ace, Card::Ace, Card::Ace, Card::Ace, Card::Ace];
        let mut my_map: HashMap<Card, i32> = HashMap::new();
        my_map.insert(Card::Ace, 5);
        let hand = Hand{ ordered: cards, buckets: my_map };

        assert_eq!(crate::day_seven::to_hand_type(hand), HandType::FiveOfAKind)
    }


}