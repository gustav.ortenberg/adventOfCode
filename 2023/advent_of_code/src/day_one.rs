fn calibration_value(p0: Option<String>) -> Option<u32> {
    match p0 {
        Some(line) => {
            let first_digit = line.chars().find(|c| c.is_digit(10));
            let last_digit = line.chars().rev().find(|c| c.is_digit(10));

            match (first_digit, last_digit) {
                (Some(first), Some(last)) => {
                    let first_digit_int = first.to_digit(10)?;
                    let last_digit_int = last.to_digit(10)?;
                    Some(first_digit_int*10 + last_digit_int)
                }
                _ => None
            }
        }
        _ => None
    }

}

fn find_total_calibration_value(p0: &str) -> u32 {
    let lines: Vec<&str> = p0.lines().collect();
    return  lines.iter()
        .filter_map(|&line| calibration_value(Some(line.to_string())))
        .sum();
}

pub fn day_one_first_part() {
    match crate::util::read_file_to_string("C:\\extra\\adventOfCode\\2023\\advent_of_code\\src\\day_one_input"){
        Ok(content) => {
            println!("Day 1 Part 1 result: {}", find_total_calibration_value(&content))
        }
        Err(err) => {eprintln!("Error reading file: {}", err)}
    };
}

fn convert_text_to_numbers(p0: &str) -> Option<String> {
    let search_strings = ["one", "two", "three", "four", "five", "six", "seven", "eight", "nine"];
    let first_match = find_first_occurrence(p0, search_strings);
    let second_match = find_last_occurrence(p0, search_strings);

    match (first_match, second_match){
        (Some(first), Some(second)) =>{
            Some(prepend_x_to_last_occurrence(prepend_x_to_first_occurrence(p0, first).as_str(), second))
        }
        _ => Some(p0.to_string())
    }
}

fn prepend_x_to_first_occurrence(input_str: &str, word: &str) -> String {
    if let Some(pos) = input_str.find(word) {
        input_str[..pos].to_string() + word_to_number(word) + &input_str[pos..]
    } else {
        input_str.to_string()
    }
}

fn prepend_x_to_last_occurrence(input_str: &str, word: &str) -> String {
    if let Some(pos) = input_str.rfind(word) {
        input_str[..pos].to_string() + word_to_number(word) + &input_str[pos..]
    } else {
        input_str.to_string()
    }
}

fn word_to_number(word: &str) -> &str {
    match word {
        "one" => "1",
        "two" => "2",
        "three" => "3",
        "four" => "4",
        "five" => "5",
        "six" => "6",
        "seven" => "7",
        "eight" => "8",
        "nine" => "9",
        _ => word
    }
}

fn find_first_occurrence<'a>(main_string: &'a str, search_strings: [&'a str; 9]) -> Option<&'a str> {
    search_strings
        .iter()
        .filter_map(|&search_str| main_string.find(search_str).map(|pos| (search_str, pos)))
        .min_by_key(|&(_, pos)| pos)
        .map(|(earliest_str, _)| earliest_str)
}

fn find_last_occurrence<'a>(main_string: &'a str, search_strings: [&'a str; 9]) -> Option<&'a str> {
    search_strings
        .iter()
        .filter_map(|&search_str| main_string.rfind(search_str).map(|pos| (search_str, pos)))
        .max_by_key(|&(_, pos)| pos)
        .map(|(last_str, _)| last_str)
}


fn find_improved_total_calibration_value(p0: &str) -> u32{

        let lines: Vec<&str> = p0.lines().collect();
        return  lines.iter()
            .filter_map(|&line| calibration_value(convert_text_to_numbers(line)))
            .sum();

}

pub fn day_one_second_part(){
    match crate::util::read_file_to_string("C:\\extra\\adventOfCode\\2023\\advent_of_code\\src\\day_one_input"){
        Ok(content) => {
            println!("Day 1 Part 2 result: {}", find_improved_total_calibration_value(&content))
        }
        Err(err) => {eprintln!("Error reading file: {}", err)}
    };
}

#[cfg(test)]
mod tests {
    use test_case::test_case;

    #[test_case("1abc2", 12 ; "first sample")]
    #[test_case("pqr3stu8vwx", 38 ; "second sample")]
    #[test_case("a1b2c3d4e5f", 15 ; "third sample")]
    #[test_case("treb7uchet", 77 ; "fourth sample")]
    fn test_calibration_value_should_be_the_first_and_last_digit(input: &str, expected: u32) {
        assert_eq!(crate::day_one::calibration_value(Some(input.to_string())), Some(expected));
    }

    #[test]
    fn test_sample_row_two() {
        assert_eq!(crate::day_one::calibration_value(Some("pqr3stu8vwx".to_string())), Some(38));
    }

    #[test]
    fn test_sample_row_three() {
        assert_eq!(crate::day_one::calibration_value(Some("a1b2c3d4e5f".to_string())), Some(15));
    }

    #[test]
    fn test_sample_row_four() {
        assert_eq!(crate::day_one::calibration_value(Some("treb7uchet".to_string())), Some(77));
    }

    #[test]
    fn test_sample_total(){
        let input = "1abc2
pqr3stu8vwx
a1b2c3d4e5f
treb7uchet";
        assert_eq!(crate::day_one::find_total_calibration_value(input), 142);
    }

    #[test]
    fn test_sample_numbers_in_text(){
        assert_eq!(crate::day_one::convert_text_to_numbers("two1nine"), Some("2two19nine".to_string()));
    }

    #[test]
    fn test_reddit_sample_numbers_in_text(){
        assert_eq!(crate::day_one::convert_text_to_numbers("eighthree"), Some("8eigh3three".to_string()))
    }

    #[test]
    fn test_sample_second_part_total(){
        let input = "two1nine
eightwothree
abcone2threexyz
xtwone3four
4nineeightseven2
zoneight234
7pqrstsixteen";

        assert_eq!(crate::day_one::find_improved_total_calibration_value(input), 29+83+13+24+42+14+76)
    }

    #[test]
    fn test_second_part_total_should_work_for_both_new_and_old_calibration_lines(){
        let input = "7pqrstsixteen
1abc2";

        assert_eq!(crate::day_one::find_improved_total_calibration_value(input), 76+12)
    }

    #[test]
    fn test_if_only_one_number_word_in_a_string_it_gets_registered_twice_which_is_okay_since_it_has_to_be_paired_with_a_number_either_before_or_after_it_but_it_will_look_strange(){
        assert_eq!(crate::day_one::convert_text_to_numbers("two"), Some("22two".to_string()))
    }
}
