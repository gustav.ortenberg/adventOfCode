object Day1 : Day {
    private fun convertToDiffList(depthList: List<Int>): List<Int>{
        var old = depthList.first()
        return depthList.map { if (old == it) {0} else { val diff = it - old; old = it; diff} }.drop(1)
    }

    override fun solution1(input: List<String>): String {
        val input = getInput().map { it.toInt() }
        val diffs = convertToDiffList(input)

        return diffs.count { it > 0 }.toString()
    }

    private fun convertToTripleSum(depthList: List<Int>): List<Int>{
        var oldest = depthList.first()
        var old = depthList.drop(1).first()
        var init = 0

        return depthList.map { if (init < 2) {init++; 0} else {val sum = oldest + old + it; oldest = old; old = it; sum} }.drop(2)
    }

    override fun solution2(input: List<String>): String {
        val input = getInput().map { it.toInt() }
        val sums = convertToTripleSum(input)
        val diffs = convertToDiffList(sums)

        return diffs.count { it > 0 }.toString()
    }
}