import kotlin.math.pow

object Day3 : Day {
    data class DiagnosticsReport(val logRows: List<String>)

    fun convertReportToGammaEpsilon(report: DiagnosticsReport): Pair<Int, Int>{
        val gammaPrecursor = convertReportToRelative(report)
            .fold(List(report.logRows.first().length){0}){acc, ints -> acc.zip(ints).map { (x, y) -> x + y }}
        val binGamma = gammaPrecursor.map { if (it > 0) 1 else 0 }
        val binEpsilon = binGamma.map { if (it == 1) 0 else 1 }
        val gamma = binGamma.fold(0){acc, i -> acc * 2 + i}
        val epsilon = binEpsilon.fold(0){acc, i -> acc * 2 + i}
        return Pair(gamma, epsilon)
    }

    fun convertReportToRelative(report: DiagnosticsReport): List<List<Int>>{
        return report.logRows.map { it.map { c -> c.digitToInt() }.map { i -> if (i == 0) -1 else 1 } }
    }

    fun binToInt(bin : List<Int>): Int{
        return bin.fold(0){acc, i -> acc * 2 + i}
    }

    override fun solution1(input: List<String>): String {
        val diagnosticsReport = DiagnosticsReport(getInput())
        val gammaEpsilon = convertReportToGammaEpsilon(diagnosticsReport)
        return gammaEpsilon.first.times(gammaEpsilon.second).toString()
    }

    fun findOxygenGeneratorRating(report: DiagnosticsReport): Int{
        val relativeReport = convertReportToRelative(report)
        val mostCommonRow = findMostCommonLogRow(relativeReport).map { if(it == -1) 0 else 1 }
        return binToInt(mostCommonRow)
    }

    fun findMostCommonLogRow(report: List<List<Int>>, searchPos: Int = 0): List<Int>{
        if (report.size == 1 || searchPos == report.first().size ) return report.first()
        val mostCommonInPos = mostCommonInPos(report, searchPos)
        return findMostCommonLogRow(report.filter { it[searchPos] == mostCommonInPos }, searchPos + 1)
    }

    fun mostCommonInPos(intMatrix: List<List<Int>>, position: Int): Int{
        return if (intMatrix.fold(0){acc, ints -> acc + ints[position]} >= 0) 1 else -1
    }

    fun findCo2ScrubberRating(report: DiagnosticsReport): Int {
        val relativeReport = convertReportToRelative((report))
        val leastCommonRow = findLeastCommonLogRow(relativeReport).map { if(it == -1) 0 else 1 }
        return binToInt(leastCommonRow)
    }

    fun findLeastCommonLogRow(report: List<List<Int>>, searchPos: Int = 0): List<Int>{
        if (report.size == 1 || searchPos == report.first().size) return report.first()
        val leastCommonInPos = leastCommonInPos(report, searchPos)
        return findLeastCommonLogRow(report.filter { it[searchPos] == leastCommonInPos }, searchPos + 1)
    }

    fun leastCommonInPos(intMatrix: List<List<Int>>, position: Int): Int{
        return if(intMatrix.fold(0){acc, ints -> acc + ints[position]} < 0) 1 else -1
    }

    override fun solution2(input: List<String>): String {
        val diagnosticsReport = DiagnosticsReport(getInput())
        return findOxygenGeneratorRating(diagnosticsReport).times(findCo2ScrubberRating(diagnosticsReport)).toString()
    }
}