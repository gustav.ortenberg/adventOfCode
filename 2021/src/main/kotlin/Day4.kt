object Day4 : Day {

    data class BingoBoard (val rows: List<Set<Int>>, val cols: List<Set<Int>>)

    fun parseBingoBoard (rows: List<String>): BingoBoard{
        return BingoBoard(listOf(setOf(1)), listOf(setOf(1)))
    }

    fun markBingoBoard (board: BingoBoard, numberToMark: Int): BingoBoard{
        return BingoBoard(board.rows.map { it.minus(numberToMark) }, board.cols.map { it.minus(numberToMark) })
    }

    fun hasBingoBoardWon (board: BingoBoard): Boolean {
        board.rows.forEach { if (it.isEmpty()) return true }
        board.cols.forEach { if (it.isEmpty()) return true }
        return false
    }

    fun scoreBingoBoard (board: BingoBoard, winningNumber: Int): Int {
        val boardSum = board.cols.fold(0){aL, sets -> aL + sets.fold(0){acc, i -> acc + i} }
        return boardSum * winningNumber
    }

    override fun solution1(input: List<String>): String {
        val input = getInput()
        //Split of bingo program
        //Loop and create all bingo boards
        //Run bingo program on all bingo boards
        //Return winning boards score
        return ""
    }
}