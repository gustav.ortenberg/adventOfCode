fun main(args: Array<String>) {
    val days = listOf(
        Day1,
        Day2,
        Day3
    )

    days.forEach { it.run() }
}