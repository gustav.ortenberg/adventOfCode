object Day2 : Day {

    enum class Direction {FORWARD, DOWN, UP}
    data class SubmarineCommand(val direction: Direction, val distance: Int){
        override fun toString(): String {
            return "$direction:$distance"
        }
    }
    data class SubmarinePosition(val horizontal: Int, val depth: Int, val aim: Int)

    private fun rowToSubmarineCommand(row: String): SubmarineCommand{
        return SubmarineCommand(
            Direction.valueOf(row.split(' ').first().uppercase()),
            row.split(' ').last().toInt())
    }

    private fun calculateEndPosition(
        startPosition: SubmarinePosition,
        commands: List<SubmarineCommand>,
        updateFunction: (SubmarinePosition, SubmarineCommand) -> SubmarinePosition
    ): SubmarinePosition{
        return if (commands.isEmpty()) {
            startPosition
        } else {
            calculateEndPosition(updateFunction(startPosition, commands.first()), commands.drop(1), updateFunction)
        }
    }

    private fun updatePositionV1(startPosition: SubmarinePosition, command: SubmarineCommand): SubmarinePosition{
        return when (command.direction) {
            Direction.FORWARD -> SubmarinePosition(
                startPosition.horizontal + command.distance,
                startPosition.depth,
                startPosition.aim
            )
            Direction.DOWN -> SubmarinePosition(startPosition.horizontal, startPosition.depth + command.distance, startPosition.aim)
            Direction.UP ->  SubmarinePosition(startPosition.horizontal, startPosition.depth - command.distance, startPosition.aim)
        }
    }

    override fun solution1(input: List<String>): String {
        val commands = input.map { rowToSubmarineCommand(it) }
        val endPosition = calculateEndPosition(SubmarinePosition(0,0, 0), commands, ::updatePositionV1)
        return (endPosition.horizontal * endPosition.depth).toString()
    }

    private fun updatePositionV2(startPosition: SubmarinePosition, command: SubmarineCommand): SubmarinePosition{
        return when (command.direction){
            Direction.DOWN -> SubmarinePosition(startPosition.horizontal, startPosition.depth, startPosition.aim + command.distance)
            Direction.UP -> SubmarinePosition(startPosition.horizontal, startPosition.depth, startPosition.aim - command.distance)
            Direction.FORWARD -> SubmarinePosition(
                startPosition.horizontal + command.distance,
                startPosition.depth + (startPosition.aim * command.distance),
                startPosition.aim
            )
        }
    }

    override fun solution2(input: List<String>): String {
        val commands = input.map { rowToSubmarineCommand(it) }
        val endPosition = calculateEndPosition(SubmarinePosition(0,0,0), commands, ::updatePositionV2)
        return (endPosition.horizontal * endPosition.depth).toString()
    }

}