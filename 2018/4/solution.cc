#include <iostream>
#include <fstream>
#include <sstream>
#include <string>
#include <unordered_map>
using namespace std;

int main(int argc, char const *argv[]) {
  if (argc != 2)
    return 1;
  list<string> logs;
  string entry;
  ifstream file (argv[1]);
  if (file.is_open()){
    while (getline(file, entry)){
      logs.push_back(entry);
    }
  }
  //Construct Sleep Schedule
  logs.sort();
  string part, min, action, id, iD;
  unordered_map<string, array<int,60>> sleepyMinute;
  while (logs.size()) {
    string entry = logs.front();
    logs.pop_front();
    istringstream sEntry (entry);
    int startTime, endTime;
    int i = 0;
    while (getline(sEntry, part, ' ')) {
      switch (i) {
        case 1: min = part.substr(3,2); break;
        case 2: action = part; break;
        case 3: id = part; break;
      }
      i++;
    }
    if (action == "Guard") iD = id;
    if (action == "falls") startTime = stoi(min);
    if (action == "wakes") {
      endTime = stoi(min);
      for (int i = startTime; i < endTime; i++) {
        array<int,60> a = {};
        a[i] = 1;
        if (!sleepyMinute.emplace(iD,a).second)
          sleepyMinute.at(iD)[i] += 1;
      }
    }
  }
  //Part 1 calculations
  string mostTiredID;
  int longestSleep = 0;
  for (auto x : sleepyMinute) {
    int sleep = 0;
    for (int i = 0; i < 60; i++)
      sleep += x.second[i];
    if (sleep > longestSleep){
      longestSleep = sleep;
      mostTiredID = x.first;
    }
  }
  int bestMin, most = 0;
  for (int i = 0; i < 60; i++) {
    if (sleepyMinute.at(mostTiredID)[i] > most) {
      most = sleepyMinute.at(mostTiredID)[i];
      bestMin = i;
    }
  }
  //Part 2 calculations
  string frequentSleepID;
  int frequentMin, maxOccurance = 0;
  for (auto x : sleepyMinute) {
    for (int i = 0; i < 60; i++) {
      if (x.second[i] > maxOccurance) {
        maxOccurance = x.second[i];
        frequentMin = i;
        frequentSleepID = x.first;
      }
    }
  }
  cout << "Part 1: " << bestMin * stoi(mostTiredID.substr(1)) << "\nPart 2: "
    << frequentMin * stoi(frequentSleepID.substr(1)) << "\n";
}
