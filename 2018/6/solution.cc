#include <iostream>
#include <fstream>
#include <sstream>
#include <string>
#include <unordered_map>
#include <unordered_set>
using namespace std;

int main(int argc, char const *argv[]) {
  if (argc != 2)
    return 1;
  int largestArea = 0;
  int i = 1;
  int maxX = 0;
  int maxY = 0;
  int region = 0;
  string line;
  unordered_map<int, pair<int,int>> sources;
  unordered_map<int, int> area;
  unordered_set<int> disc;
  ifstream file(argv[1]);
  if (file.is_open()){
    while(getline(file, line)) {
      int x = stoi(line.substr(0, line.find(", ")));
      int y = stoi(line.substr(line.find(", ")+1));
      if (x > maxX) maxX = x;
      if (y > maxY) maxY = y;
      sources.emplace(i++, make_pair(x, y));
    }

    for (int i = 1; i <= maxX; i++) {
      for (int j = 1; j <= maxY; j++) {
        unordered_map<int, int> round = {};
        for (auto x : sources) {
          int manhattan = 0;
          manhattan += abs(x.second.first-i);
          manhattan += abs(x.second.second-j);
          round.emplace(x.first, manhattan);
        }
        int id;
        bool alone = true;
        int shortest = 100000;
        for (auto x : round) {
          if (x.second < shortest) {
            shortest = x.second;
            id = x.first;
            alone = true;
          } else if (x.second == shortest) {
            alone = false;
          }
        }
        if (alone) {
          if (area.find(id) == area.end()) {
            area.emplace(id,1);
          } else area.at(id) += 1;
          if (i == 1 || j == 1 || i == maxX || j == maxY) {
            disc.emplace(id);
          }
        }
      }
    }
    for (auto x : area) {
      if (disc.find(x.first) == disc.end() && x.second > largestArea){
        cout << "id: " << x.first << " : " << x.second << "\n";
        largestArea = x.second;
      }
    }
    //Part 2
    for (int i = 1; i <= maxX;i++) {
      for (int j = 1; j <= maxY;j++) {
        //if manhattan < 10000 (to all sources) => +1 in region
        int manhattan = 0;
        for (auto x : sources)
          manhattan += abs(x.second.first-i) + abs(x.second.second-j);
        if (manhattan < 10000) region++;
      }
    }
    file.close();
  }

  cout << "Part 1: " << largestArea << "\nPart 2: " << region << "\n";
  return 0;
}
