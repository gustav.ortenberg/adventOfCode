#include <iostream>
#include <fstream>
#include <string>
#include <unordered_set>
using namespace std;

//Takes 1 file as agument content on the form:
//  - 1
//  + 6
//  ... (etc.)
//returns summation of the file and the first frequency that is encountered
//twice when looping the instructions in the file
int main(int argc, char const *argv[]) {
  int frequency = 0;
  int twice;
  int f = 0;
  bool found = false;
  unordered_set<int> visited = {0};
  unordered_set<int>::const_iterator got;
  if (argc != 2)
    return 1;
  string change;
  ifstream file (argv[1]);
  //Part 1 Summation
  if (file.is_open()) {
    while (getline(file, change)) {
      frequency += (('-' == change[0]) ? -1 : 1) * stoi(change.substr(1));
    }
    file.close();
    //Part 2 First number encountered twice when looping the instructions
    while (!found) {
      ifstream file (argv[1]);
      if (file.is_open()) {
        while (getline(file, change)) {
          f += (('-' == change[0]) ? -1 : 1 )* stoi(change.substr(1));
          got = visited.find(f);
          if (!found && got != visited.end()){
            twice = f;
            found = true;
          }
          visited.insert(f);
        }
        file.close();
      }
    }
  }
  printf("Part1: %d\nPart2: %d\n", frequency, twice);
  return 0;
}
