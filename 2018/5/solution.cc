#include <iostream>
#include <fstream>
#include <sstream>
#include <string>
#include <unordered_map>
using namespace std;

int main(int argc, char const *argv[]) {
  if (argc != 2)
    return 1;
  ifstream file (argv[1]);
  string polymer;
  int shrt;
  int partOne;
  if (file.is_open()) {
    getline(file, polymer);
    int lenPoly = polymer.size();
    for(int i = 1; i < lenPoly; i++){
      if (polymer[i-1] != polymer[i] && tolower(polymer[i-1]) == tolower(polymer[i])){
        polymer.erase(i-1,2);
        i = 0;
      }
    }
    partOne = polymer.size();
    shrt = partOne;
    for (char x : {'a','b','c','d','e','f','g','h','i','j','k','l','m','n','o','p','q','r','s','t','u','v','w','x','y','z'}) {
      string copy = polymer;
      int copyLen = copy.size();
      for (int i = 0; i < copyLen; i++) {
        if (tolower(copy[i]) == x) {
          copy.erase(i,1);
          i--;
        }
      }
      for(int i = 1; i < copyLen; i++){
        if (copy[i-1] != copy[i] && tolower(copy[i-1]) == tolower(copy[i])){
          copy.erase(i-1,2);
          i = 0;
        }
      }
      copyLen = copy.size();
      if (copyLen < shrt) shrt = copyLen;
    }
    file.close();
  }
  cout << "Part 1: " << partOne << "\nPart 2: " << shrt <<"\n";
  return 0;
}
