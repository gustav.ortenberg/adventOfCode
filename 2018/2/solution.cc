#include <iostream>
#include <fstream>
#include <string>
#include <unordered_map>
#include <unordered_set>
using namespace std;

int charDiff (string s0, string s1){
  if (s0.size() == 0 || s1.size() == 0) return 0;
  return charDiff(s0.substr(1), s1.substr(1)) + ((s0[0] == s1[0]) ? 0 : 1);
}

string charToStr(char c){
  string s(1,c);
  return s;
}

string inCommon (string s0, string s1){
  if (s0.size() == 0 || s1.size() == 0) return "";
  return ((s0[0] == s1[0]) ? charToStr(s0[0]) : "") + inCommon(s0.substr(1), s1.substr(1));
}

int main(int argc, char const *argv[]) {
  if (argc != 2)
    return 1;
  string id;
  unordered_map<char,int> letterCount;
  unordered_set<string> ids;
  ifstream file (argv[1]);
  int checksum, twos, threes;
  twos = threes = 0;
  if (file.is_open()) {
    //Part 1: Count the number of words with exactly 2 respectively 3 identical
    //letters and multiply the number of occurences together
    while (getline(file, id)) {
      bool two = false, three = false;
      ids.emplace(id);
      for (char letter : id){
        if (letterCount.find(letter) != letterCount.end()) {
          letterCount.at(letter) += 1;
        } else {
          letterCount.emplace(letter, 1);
        }
      }
      for (char letter : id) {
        if (letterCount.at(letter) == 2) two = true;
        if (letterCount.at(letter) == 3) three = true;
      }
      if (two) twos++;
      if (three) threes++;
      letterCount.clear();
    }
    file.close();
  }
  checksum = twos * threes;
  //Part 2: Find the common letters in the two ids that only differ by one char
  string common;
  for (auto x : ids) {
    for (auto y : ids){
      if (charDiff(x, y) == 1) {
        common = inCommon(x,y);
      }
    }
  }
  cout << "Part 1: " << checksum << "\nPart 2: " << common << "\n";
  return 0;
}
