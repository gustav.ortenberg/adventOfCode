#include <iostream>
#include <fstream>
#include <sstream>
#include <string>
#include <unordered_map>
using namespace std;

int main(int argc, char const *argv[]) {
  if (argc != 2)
    return 1;
  unordered_map<string, bool> grid;
  ifstream file (argv[1]);
  string claim, part, val, aClaim, goodClaim;
  string parts[4];
  //Part 1
  if (file.is_open()) {
    while (getline(file, claim)) {
      //Monsterous parsing
      int i = 0;
      istringstream claimS (claim);
      while(getline(claimS, part, ' ')){
        if (i == 2) {
          part.pop_back();
          i -= 2;
          istringstream partS (part);
          while(getline(partS, val, ','))
            parts[i++] = val;
        }
        if (i == 3){
          i--;
          istringstream partS (part);
          while(getline(partS, val, 'x'))
            parts[i++] = val;
        }
        i++;
      }
      int x = stoi(parts[0]);
      int y = stoi(parts[1]);
      int xPlus = stoi(parts[2]);
      int yPlus = stoi(parts[3]);
      for (int i = 0; i < xPlus; i++) {
        for (int j = 0; j < yPlus; j++) {
          string pos = to_string(x+i) + "," + to_string(y+j);
          if (grid.find(pos) != grid.end()) grid.at(pos) = true;
          else grid.emplace(pos,false);
        }
      }
    }
    file.close();
    
    //Part 2
    ifstream file (argv[1]);
    if (file.is_open()) {
      while (getline(file, claim)) {
        //Monsterous parsing
        int i = 0;
        istringstream claimS (claim);
        while(getline(claimS, part, ' ')){
          if (i == 0) {
            aClaim = part.substr(1);
          }
          if (i == 2) {
            part.pop_back();
            i -= 2;
            istringstream partS (part);
            while(getline(partS, val, ','))
              parts[i++] = val;
          }
          if (i == 3){
            i--;
            istringstream partS (part);
            while(getline(partS, val, 'x'))
              parts[i++] = val;
          }
          i++;
        }
        int x = stoi(parts[0]);
        int y = stoi(parts[1]);
        int xPlus = stoi(parts[2]);
        int yPlus = stoi(parts[3]);
        bool intersect = false;
        for (int i = 0; i < xPlus; i++) {
          for (int j = 0; j < yPlus; j++) {
            string pos = to_string(x+i) + "," + to_string(y+j);
            if (grid.at(pos)) intersect = true;
          }
        }
        if (!intersect) goodClaim = aClaim;
      }
      file.close();
    }
  }
  int overlapping = 0;
  for (auto x : grid) {
    if (x.second) overlapping++;
  }
  cout << "Part 1: " << overlapping << "\nPart 2: " << goodClaim << "\n";
  return 0;
}
