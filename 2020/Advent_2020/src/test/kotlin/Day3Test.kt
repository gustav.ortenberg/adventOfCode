import com.google.common.truth.Truth.assertThat
import org.junit.Test

internal class Day3Test {

    private fun createTestMap(): Day3.TreeMap{
        val mapAsString = "..##.......\n" +
                "#...#...#..\n" +
                ".#....#..#.\n" +
                "..#.#...#.#\n" +
                ".#...##..#.\n" +
                "..#.##.....\n" +
                ".#.#.#....#\n" +
                ".#........#\n" +
                "#.##...#...\n" +
                "#...##....#\n" +
                ".#..#...#.#"
        return Day3.TreeMap(mapAsString.split(Regex("\n")).map { it.toList() }, 11)
    }

    @Test
    fun `It should correctly identify a tree`(){
        assertThat(Day3.isTree('.')).isFalse()
        assertThat(Day3.isTree('#')).isTrue()
    }

    @Test
    fun `It should find the correct number of encountered trees`() {
        val map = createTestMap()
        assertThat(Day3.encounteredTrees(Day3.Slope(1,1), map)).isEqualTo(2)
        assertThat(Day3.encounteredTrees(Day3.Slope(3,1), map)).isEqualTo(7)
        assertThat(Day3.encounteredTrees(Day3.Slope(5,1), map)).isEqualTo(3)
        assertThat(Day3.encounteredTrees(Day3.Slope(7,1), map)).isEqualTo(4)
        assertThat(Day3.encounteredTrees(Day3.Slope(1,2), map)).isEqualTo(2)
    }

}