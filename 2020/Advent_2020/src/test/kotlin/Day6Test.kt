import com.google.common.truth.Truth.assertThat
import org.junit.Test

internal class Day6Test {

    @Test
    fun `questionnaireUnion should yield the correct count for a group of travelers`() {
        assertThat(
            Day6.questionnaireUnion(
                listOf(
                    "abcx",
                    "abcy",
                    "abcz"
                )
            )
        ).isEqualTo(6)
    }

    @Test
    fun `solution1 should return the sum of the union for each group of travelers`() {
        assertThat(
            Day6.solution1(
                listOf(
                    "abc", "",
                    "a", "b", "c", "",
                    "ab", "ac", "",
                    "a", "a", "a", "a", "",
                    "b"
                )
            )
        ).isEqualTo("11")
    }

    @Test
    fun `solution2 should return the sum of the intersecting answers in each group of travelers`() {
        assertThat(
            Day6.solution2(
                listOf(
                    "abc", "",
                    "a", "b", "c", "",
                    "ab", "ac", "",
                    "a", "a", "a", "a", "",
                    "b"
                )
            )
        ).isEqualTo("6")
    }
}