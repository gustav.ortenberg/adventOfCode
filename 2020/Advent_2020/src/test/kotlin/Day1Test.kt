import com.google.common.truth.Truth.assertThat
import org.junit.Test

internal class Day1Test {

    @Test
    fun `Should return a pair that sums up to the desired sum`() {
        val list = listOf(1721, 979, 366, 299, 675, 1456)
        val result = Day1.findPairSum(2020, list)
        assertThat(result).isAnyOf(Pair(1721, 299), Pair(299, 1721))
    }


    @Test
    fun `Should return an n-tuple that sums up to the desired sum`() {
        val list = listOf(1721, 979, 366, 299, 675, 1456)
        val result = Day1.findTripleSum(2020, list)
        assertThat(result).isAnyOf(
            Triple(979, 366, 675),
            Triple(979, 675, 366),
            Triple(366, 675, 979),
            Triple(366, 979, 675),
            Triple(675, 366, 979),
            Triple(675, 979, 366)
        )
    }

}

