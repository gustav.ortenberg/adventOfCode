import org.junit.Test
import com.google.common.truth.Truth.assertThat



internal class Day8Test {

    private fun TestProgram(): Day8.Program {
        val operations = listOf(
            Day8.Operation("nop", '+', 0),
            Day8.Operation("acc", '+', 1),
            Day8.Operation("jmp", '+', 4),
            Day8.Operation("acc", '+', 3),
            Day8.Operation("jmp", '-', 3),
            Day8.Operation("acc", '-', 99),
            Day8.Operation("acc", '+', 1),
            Day8.Operation("jmp", '-', 4),
            Day8.Operation("acc", '+', 6)
        )
        return Day8.Program(operations, 0)
    }

    @Test
    fun `name`(){
        assertThat(TestProgram()).
    }

}