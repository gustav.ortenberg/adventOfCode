import com.google.common.truth.Truth.assertThat
import org.junit.Test

internal class Day4Test {

    //ecl:gry pid:860033327 eyr:2020 hcl:#fffffd
    //byr:1937 iyr:2017 cid:147 hgt:183cm
    private fun passport1(): Day4.Passport {
        return Day4.Passport(
            1937,
            2017,
            2020,
            Day4.Height(183, "cm"),
            "#fffffd",
            "gry",
            "860033327",
            147
        )
    }

    //iyr:2013 ecl:amb cid:350 eyr:2023 pid:028048884
    //hcl:#cfa07d byr:1929
    private fun passport2(): Day4.Passport {
        return Day4.Passport(
            1929,
            2013,
            2023,
            null,
            "#cfa07d",
            "amb",
            "028048884",
            350
        )
    }

    //hcl:#ae17e1 iyr:2013
    //eyr:2024
    //ecl:brn pid:760753108 byr:1931
    //hgt:179cm
    private fun passport3(): Day4.Passport {
        return Day4.Passport(
            1931,
            2013,
            2024,
            Day4.Height(179, "cm"),
            "#ae17e1",
            "brn",
            "760753108",
            null
        )
    }

    //hcl:#cfa07d eyr:2025 pid:166559648
    //iyr:2011 ecl:brn hgt:59in
    private fun passport4(): Day4.Passport {
        return Day4.Passport(
            null,
            2011,
            2025,
            Day4.Height(59, "in"),
            "#cfa07d",
            "brn",
            "166559648",
            null
        )
    }

    @Test
    fun `It should parse height correctly`() {
        assertThat(Day4.stringToHeight("130cm")).isEqualTo(Day4.Height(130, "cm"))
        assertThat(Day4.stringToHeight("40in")).isEqualTo(Day4.Height(40, "in"))
    }

    @Test
    fun `It should parse a passport correctly`() {
        val fields = listOf(
            Pair("ecl", "gry"),
            Pair("pid", "860033327"),
            Pair("eyr", "2020"),
            Pair("hcl", "#fffffd"),
            Pair("byr", "1937"),
            Pair("iyr", "2017"),
            Pair("cid", "147"),
            Pair("hgt", "183cm")
        )
        val expectedPassport = passport1()
        assertThat(Day4.createPassportFromFields(fields)).isEqualTo(expectedPassport)
    }

    @Test
    fun `It should recognize valid passports`() {
        assertThat(Day4.isValidPassport(passport1())).isTrue()
        assertThat(Day4.isValidPassport(passport3())).isTrue()
    }

    @Test
    fun `It should recognize invalid passports`() {
        assertThat(Day4.isValidPassport(passport2())).isFalse()
        assertThat(Day4.isValidPassport(passport4())).isFalse()
    }

    @Test
    fun `It should parse the batch file correctly`() {
        val input = ("ecl:gry pid:860033327 eyr:2020 hcl:#fffffd\n" +
                "byr:1937 iyr:2017 cid:147 hgt:183cm\n" +
                "\n" +
                "iyr:2013 ecl:amb cid:350 eyr:2023 pid:028048884\n" +
                "hcl:#cfa07d byr:1929\n" +
                "\n" +
                "hcl:#ae17e1 iyr:2013\n" +
                "eyr:2024\n" +
                "ecl:brn pid:760753108 byr:1931\n" +
                "hgt:179cm\n" +
                "\n" +
                "hcl:#cfa07d eyr:2025 pid:166559648\n" +
                "iyr:2011 ecl:brn hgt:59in").split(Regex("\n"))

        val expected = listOf(
            listOf(
                Pair("ecl", "gry"),
                Pair("pid", "860033327"),
                Pair("eyr", "2020"),
                Pair("hcl", "#fffffd"),
                Pair("byr", "1937"),
                Pair("iyr", "2017"),
                Pair("cid", "147"),
                Pair("hgt", "183cm")
            ),
            listOf(
                Pair("iyr", "2013"),
                Pair("ecl", "amb"),
                Pair("cid", "350"),
                Pair("eyr", "2023"),
                Pair("pid", "028048884"),
                Pair("hcl", "#cfa07d"),
                Pair("byr", "1929")
            ),
            listOf(
                Pair("hcl", "#ae17e1"),
                Pair("iyr", "2013"),
                Pair("eyr", "2024"),
                Pair("ecl", "brn"),
                Pair("pid", "760753108"),
                Pair("byr", "1931"),
                Pair("hgt", "179cm")
            ),
            listOf(
                Pair("hcl", "#cfa07d"),
                Pair("eyr", "2025"),
                Pair("pid", "166559648"),
                Pair("iyr", "2011"),
                Pair("ecl", "brn"),
                Pair("hgt", "59in")
            )
        )

        assertThat(Day4.parseInput(input)).isEqualTo(expected)
    }

    //eyr:1972 cid:100
    //hcl:#18171d ecl:amb hgt:170 pid:186cm iyr:2018 byr:1926
    private fun passport5(): Day4.Passport {
        return Day4.Passport(
            1926,
            2018,
            1972,
            Day4.Height(170, "N/A"),
            "#18171d",
            "amb",
            "186cm",
            100
        )
    }

    //iyr:2019
    //hcl:#602927 eyr:1967 hgt:170cm
    //ecl:grn pid:012533040 byr:1946
    private fun passport6(): Day4.Passport {
        return Day4.Passport(
            1946,
            2019,
            1967,
            Day4.Height(170, "cm"),
            "#602927",
            "grn",
            "012533040",
            null
        )
    }

    //hcl:dab227 iyr:2012
    //ecl:brn hgt:182cm pid:021572410 eyr:2020 byr:1992 cid:277
    private fun passport7(): Day4.Passport {
        return Day4.Passport(
            1992,
            2012,
            2020,
            Day4.Height(182, "cm"),
            "dab227",
            "brn",
            "021572410",
            277
        )
    }

    //hgt:59cm ecl:zzz
    //eyr:2038 hcl:74454a iyr:2023
    //pid:3556412378 byr:2007
    private fun passport8(): Day4.Passport {
        return Day4.Passport(
            2007,
            2023,
            2038,
            Day4.Height(59, "cm"),
            "74454a",
            "zzz",
            "3556412378",
            null
        )
    }

    //pid:087499704 hgt:74in ecl:grn iyr:2012 eyr:2030 byr:1980
    //hcl:#623a2f
    private fun passport9(): Day4.Passport {
        return Day4.Passport(
            1980,
            2012,
            2030,
            Day4.Height(74, "in"),
            "#623a2f",
            "grn",
            "087499704",
            null
        )
    }

    //eyr:2029 ecl:blu cid:129 byr:1989
    //iyr:2014 pid:896056539 hcl:#a97842 hgt:165cm
    private fun passport10(): Day4.Passport {
        return Day4.Passport(
            1989,
            2014,
            2029,
            Day4.Height(165, "cm"),
            "#a97842",
            "blu",
            "896056539",
            129
        )
    }

    //hcl:#888785
    //hgt:164cm byr:2001 iyr:2015 cid:88
    //pid:545766238 ecl:hzl
    //eyr:2022
    private fun passport11(): Day4.Passport {
        return Day4.Passport(
            2001,
            2015,
            2022,
            Day4.Height(164, "cm"),
            "#888785",
            "hzl",
            "545766238",
            88
        )
    }

    //iyr:2010 hgt:158cm hcl:#b6652a ecl:blu byr:1944 eyr:2021 pid:093154719
    private fun passport12(): Day4.Passport {
        return Day4.Passport(
            1944,
            2010,
            2021,
            Day4.Height(158, "cm"),
            "#b6652a",
            "blu",
            "093154719",
            null
        )
    }

    @Test
    fun `It should correctly identify really invalid passports`() {
        assertThat(Day4.isReallyValidPassport(passport5())).isFalse()
        assertThat(Day4.isReallyValidPassport(passport6())).isFalse()
        assertThat(Day4.isReallyValidPassport(passport7())).isFalse()
        assertThat(Day4.isReallyValidPassport(passport8())).isFalse()
    }

    @Test
    fun `It should correctly identify really valid passports`() {
        assertThat(Day4.isReallyValidPassport(passport9())).isTrue()
        assertThat(Day4.isReallyValidPassport(passport10())).isTrue()
        assertThat(Day4.isReallyValidPassport(passport11())).isTrue()
        assertThat(Day4.isReallyValidPassport(passport12())).isTrue()
    }
}