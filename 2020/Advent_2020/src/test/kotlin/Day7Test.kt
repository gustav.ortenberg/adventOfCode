import com.google.common.truth.Truth.assertThat
import org.junit.Test

internal class Day7Test {
    fun testInput(): List<String> {
        return listOf(
            "light red bags contain 1 bright white bag, 2 muted yellow bags.",
            "dark orange bags contain 3 bright white bags, 4 muted yellow bags.",
            "bright white bags contain 1 shiny gold bag.",
            "muted yellow bags contain 2 shiny gold bags, 9 faded blue bags.",
            "shiny gold bags contain 1 dark olive bag, 2 vibrant plum bags.",
            "dark olive bags contain 3 faded blue bags, 4 dotted black bags.",
            "vibrant plum bags contain 5 faded blue bags, 6 dotted black bags.",
            "faded blue bags contain no other bags.",
            "dotted black bags contain no other bags."
        )
    }

    private fun rule0(): Day7.BagRule{
        return Day7.BagRule("light red", listOf(Day7.BagContent("bright white", 1), Day7.BagContent("muted yellow", 2)))
    }

    private fun rule2(): Day7.BagRule{
        return Day7.BagRule("bright white", listOf(Day7.BagContent("shiny gold", 1)))
    }

    private fun rule8(): Day7.BagRule{
        return Day7.BagRule("dotted black", listOf(Day7.BagContent("no other", 0)))
    }

    @Test
    fun `parseRule should return rules showing what bags are allowed to be in a bag`() {
        assertThat(Day7.parseRule(testInput()[0])).isEqualTo(rule0())
        assertThat(Day7.parseRule(testInput()[2])).isEqualTo(rule2())
        assertThat(Day7.parseRule(testInput().last())).isEqualTo(rule8())
    }

    @Test
    fun `parseRules should return rules for which bags are a bag could be directly put in`() {
        assertThat(Day7.parseRules(testInput().subList(0, 3))["shiny gold"]).isEqualTo(mutableSetOf("bright white"))
    }

    @Test
    fun `solution1 should be 4 for the test input`() {
        assertThat(Day7.solution1(testInput())).isEqualTo("4")
    }

    @Test
    fun `bagsIn should return 32 bags in a shiny gold bag with the test rules`(){
        assertThat(Day7.bagsIn("shiny gold",testInput().map { Day7.parseRule(it) })).isEqualTo(32)
    }
}