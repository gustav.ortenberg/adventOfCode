import com.google.common.truth.Truth.assertThat
import org.junit.Test

internal class Day2Test {

    @Test
    fun `Should be accepted passwords`(){
        assertThat(Day2.okayPassword("abcde", Day2.Rule(1, 3, 'a'))).isTrue()
        assertThat(Day2.okayPassword("ccccccccc", Day2.Rule(2,9, 'c'))).isTrue()
    }

    @Test
    fun `Should not be accepted passwords`(){
        assertThat(Day2.okayPassword("cdefg", Day2.Rule(1,3,'b'))).isFalse()
    }

    @Test
    fun `Should parse a row correctly`(){
        assertThat(Day2.parseRow("1-3 a: abcde")).isEqualTo(Pair("abcde", Day2.Rule(1, 3, 'a')))
    }

    @Test
    fun `Should be accepted Toboggan passwords`(){
        assertThat(Day2.tobogganOkayPassword("abcde", Day2.Rule(1, 3, 'a'))).isTrue()
    }

    @Test
    fun `Should not be accepted Toboggan passwords`(){
        assertThat(Day2.tobogganOkayPassword("ccccccccc", Day2.Rule(2,9, 'c'))).isFalse()
        assertThat(Day2.tobogganOkayPassword("cdefg", Day2.Rule(1,3,'b'))).isFalse()
    }
}