import com.google.common.truth.Truth.assertThat
import org.junit.Test

internal class Day5Test {

    @Test
    fun `Parse boarding pass should give the correct row, column, and seat ID`(){
        assertThat(Day5.parseBoardingPass("FBFBBFFRLR")).isEqualTo(Day5.BoardingPass(44, 5, 357))
        assertThat(Day5.parseBoardingPass("BFFFBBFRRR")).isEqualTo(Day5.BoardingPass(70, 7, 567))
        assertThat(Day5.parseBoardingPass("FFFBBBFRRR")).isEqualTo(Day5.BoardingPass(14, 7, 119))
        assertThat(Day5.parseBoardingPass("BBFFBBFRLL")).isEqualTo(Day5.BoardingPass(102, 4, 820))
    }

    @Test
    fun `Solution1 finds the highest numbered seat ID`(){
        val input = listOf(
            "FBFBBFFRLR",
            "BFFFBBFRRR",
            "FFFBBBFRRR",
            "BBFFBBFRLL"
        )
        assertThat(Day5.solution1(input)).isEqualTo("820")
    }
}