object Day7 : Day {

    data class BagContent(val bagType: String, val amount: Int)

    data class BagRule(val bagType: String, val containedBags: List<BagContent>)

    fun parseRules(input: List<String>): HashMap<String, MutableSet<String>> {
        val rules = HashMap<String, MutableSet<String>>()
        input.map { parseRule(it) }.forEach {
            it.containedBags.forEach { containedBag ->
                if (rules.containsKey(containedBag.bagType))
                    rules[containedBag.bagType]!!.add(it.bagType)
                else
                    rules[containedBag.bagType] = mutableSetOf(it.bagType)
            }
        }
        return rules
    }

    fun parseRule(line: String): BagRule {
        val container = line.split(" bag")[0]
        val contained =
            line.split("""(contain)\s""".toRegex())[1]
                .split("""(\s)(bag)(s)?.(\s)?""".toRegex())
                .dropLast(1)
                .map { parseBagContent(it) }
        return BagRule(container, contained)
    }

    private fun parseBagContent(item: String): BagContent{
        return when (item) {
            "no other" -> BagContent("no other", 0)
            else -> listToBagContent(item.split("""(\s)""".toRegex()))
        }
    }

    private fun listToBagContent(list: List<String>): BagContent{
        return BagContent(list[1] + " " + list[2], list[0].toInt())
    }

    private fun findAllContainersOf(bagType: String, bagRules: HashMap<String, MutableSet<String>>): Set<String> {
        if (bagType == "no other")
            return setOf()
        if (bagRules.contains(bagType)) {
            return bagRules[bagType]!!.map { findAllContainersOf(it, bagRules) }.reduce { acc, set -> acc.union(set) }.union(bagRules[bagType]!!)
        }
        return setOf(bagType)
    }

    fun bagsIn(bagType: String, rules: List<BagRule>): Int{
        return when (bagType) {
            "no other" ->  1
            else -> rules.find { it.bagType == bagType }!!.containedBags.map { (bagsIn(it.bagType, rules) + 1) * it.amount}.sum()
        }
    }

    override fun solution1(input: List<String>): String {
        return findAllContainersOf("shiny gold", parseRules(input)).count().toString()
    }

    override fun solution2(input: List<String>): String {
        var rules = input.map { parseRule(it) }
        return bagsIn("shiny gold", rules).toString()
    }

}