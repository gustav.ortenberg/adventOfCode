fun main(args: Array<String>) {
    val days = listOf(
        Day1,
        Day2,
        Day3,
        Day4,
        Day5,
        Day6,
        Day7
    )
    days.forEach { it.run() }
}