object Day6 : Day {

    fun questionnaireUnion(answers: List<String>): Int {
        return answers.map { it.toSet() }.reduce { acc, set -> acc.union(set)}.count()
    }

    fun questionnaireIntersect(answers: List<String>): Int {
        return answers.map { it.toSet() }.reduce{ acc, set -> acc.intersect(set)}.count()
    }

    override fun solution1(input: List<String>): String {
        val groups = mutableListOf<MutableList<String>>(mutableListOf())
        input.forEach { if (it.isNotEmpty()) {groups.last().add(it)} else { groups.add(mutableListOf())} }
        return groups.map { questionnaireUnion(it) }.sum().toString()
    }

    override fun solution2(input: List<String>): String {
        val groups = mutableListOf<MutableList<String>>(mutableListOf())
        input.forEach { if (it.isNotEmpty()) groups.last().add(it) else groups.add(mutableListOf()) }
        return groups.map { questionnaireIntersect(it) }.sum().toString()
    }

}