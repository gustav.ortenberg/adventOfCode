import java.util.concurrent.atomic.DoubleAccumulator

object Day8 {

    data class Operation(val type: String, val sign: Char, val amount: Int, var visited: Boolean = false) {
        public fun visit(): Unit {
            visited = true
        }
    }

    data class Program(val operations: List<Operation>, var accumulator: Int) {

    }


}