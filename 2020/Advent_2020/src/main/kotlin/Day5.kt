object Day5 : Day {

    data class BoardingPass(
        val row: Int,
        val col: Int,
        val seatID: Int = row * 8 + col
    )

    fun parseBoardingPass(pass: String): BoardingPass {
        val rowString = pass.dropLast(3)
        val colString = pass.drop(7)

        val rowBools = rowString.map { if (it == 'B') 1 else 0 }
        val colBools = colString.map { if (it == 'R') 1 else 0 }

        val row = rowBools.reduce { acc, i -> (acc * 2 + i) }
        val col = colBools.reduce { acc, i -> (acc * 2 + i) }

        return BoardingPass(row, col)
    }

    override fun solution1(input: List<String>): String {
        return input.map { parseBoardingPass(it) }.maxByOrNull { it.seatID }?.seatID.toString()
    }

    override fun solution2(input: List<String>): String {
        val sortedInput = input.map { parseBoardingPass(it) }.sortedBy { it.seatID }
        var currentSeat = sortedInput.first().seatID
        sortedInput.forEach {
            if (it.seatID != currentSeat){
                return currentSeat.toString()
            }
            currentSeat += 1
        }
        return ""
    }
}