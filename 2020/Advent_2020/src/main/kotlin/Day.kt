interface Day {
    fun run() {
        task1Composition()
        task2Composition()
    }

    fun task1Composition() {
        println(dayName() + " task 1")
        println(solution1(getInput()))
    }

    fun task2Composition() {
        println(dayName() + " task 2")
        println(solution2(getInput()))
    }

    fun solution1(input: List<String>): String {
        return "To be implemented"
    }

    fun solution2(input: List<String>): String {
        return "To be implemented"
    }

    fun getInput(): List<String> {
        val reader = Day::class.java.classLoader.getResourceAsStream(dayNameWithoutSpace()).bufferedReader()
        return reader.lineSequence().toList()
    }

    fun dayName(): String {
        return addSpaceInDay(dayNameWithoutSpace())
    }

    fun dayNameWithoutSpace(): String {
        return this.toString().split('@').first()
    }

    fun addSpaceInDay(day: String): String {
        return day.dropLast(1) + " " + day.last()
    }
}