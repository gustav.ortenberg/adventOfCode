object Day4 : Day {

    data class Height(
        val amount: Int,
        val unit: String
    )

    data class Passport(
        val birthYear: Int?,         //byr
        val issueYear: Int?,         //iyr
        val expirationYear: Int?,    //eyr
        val height: Height?,         //hgt
        val hairColor: String?,      //hcl
        val eyeColor: String?,       //ecl
        val passportID: String?,        //pid
        val countryID: Int?          //cid
    )

    fun stringToHeight(string: String?): Height? {
        if (string == null) {
            return null;
        }
        if (string.endsWith("in")) {
            return Height(string.replace("in", "").toInt(), "in")
        } else if (string.endsWith("cm")) {
            return Height(string.replace("cm", "").toInt(), "cm")
            println("2")
        } else {
            return Height(string.toInt(), "N/A")
            //println(string)
            //throw NotImplementedError()
        }
    }

    fun createPassportFromFields(fields: List<Pair<String, String>>): Passport {
        val birthYear = fields.find { it.first == "byr" }?.second?.toInt()
        val issuesYear = fields.find { it.first == "iyr" }?.second?.toInt()
        val expirationYear = fields.find { it.first == "eyr" }?.second?.toInt()
        val height = stringToHeight(fields.find { it.first == "hgt" }?.second)
        val hairColor = fields.find { it.first == "hcl" }?.second
        val eyeColor = fields.find { it.first == "ecl" }?.second
        val passportID = fields.find { it.first == "pid" }?.second
        val countryID = fields.find { it.first == "cid" }?.second?.toInt()
        return Passport(
            birthYear,
            issuesYear,
            expirationYear,
            height,
            hairColor,
            eyeColor,
            passportID,
            countryID
        )
    }

    // Allows passports that only miss countryID
    fun isValidPassport(passport: Passport): Boolean {
        return passport.birthYear != null
                && passport.issueYear != null
                && passport.expirationYear != null
                && passport.height != null
                && passport.hairColor != null
                && passport.eyeColor != null
                && passport.passportID != null
    }

    private fun isValidByr(byr: Int): Boolean {
        return byr in 1920..2002
    }

    private fun isValidIyr(iyr: Int): Boolean {
        return iyr in 2010..2020
    }

    private fun isValidEyr(eyr: Int): Boolean {
        return eyr in 2020..2030
    }

    private fun isValidHeight(height: Height): Boolean {
        return when (height.unit) {
            "cm" -> height.amount in 150..193
            "in" -> height.amount in 59..76
            else -> false
        }
    }

    private fun isHexChar(char: Char): Boolean {
        return char in 'a'..'f' || char in '0'..'9'
    }

    private fun isValidHairColor(hairColor: String): Boolean {
        return hairColor.first() == '#'
                && hairColor.count() == 7
                && hairColor.substring(1).map { isHexChar(it) }.reduce { acc, b -> acc && b }

    }

    private fun isValidEyeColor(eyeColor: String): Boolean {
        return when (eyeColor) {
            "amb" -> true
            "blu" -> true
            "brn" -> true
            "gry" -> true
            "grn" -> true
            "hzl" -> true
            "oth" -> true
            else -> false
        }
    }

    private fun isValidPassportID(passportID: String): Boolean {
        return passportID.count() == 9 && passportID.map { it.isDigit() }.reduce { acc, b -> acc && b }
    }

    fun isReallyValidPassport(passport: Passport): Boolean {
        return isValidPassport(passport)
                && isValidByr(passport.birthYear!!)
                && isValidIyr(passport.issueYear!!)
                && isValidEyr(passport.expirationYear!!)
                && isValidHeight(passport.height!!)
                && isValidHairColor(passport.hairColor!!)
                && isValidEyeColor(passport.eyeColor!!)
                && isValidPassportID(passport.passportID!!)
    }

    private fun listToPair(list: List<String>): Pair<String, String> {
        return Pair(list.first(), list.last())
    }

    //Not a very nice solution... but it works :)
    fun parseInput(input: List<String>): List<List<Pair<String, String>>> {
        var acc = ""
        val list = mutableListOf<List<Pair<String, String>>>()
        input.forEach {
            if (it == "") {
                list.add(acc.split(' ').dropLast(1).map { listToPair(it.split(':')) })
                acc = ""
            } else {
                acc += "$it "
            }
        }
        list.add(acc.split(' ').dropLast(1).map { listToPair(it.split(':')) })
        return list.toList()
    }

    override fun solution1(input: List<String>): String {
        val parsedInput = parseInput(input)
        val passports = parsedInput.map { createPassportFromFields(it) }
        val validPassports = passports.map { isValidPassport(it) }.count { it }
        return validPassports.toString()
    }
    override fun solution2(input: List<String>): String {
        val parsedInput = parseInput(input)
        val passports = parsedInput.map { createPassportFromFields(it) }
        val validPassports = passports.map { isReallyValidPassport(it) }.count { it }
        return validPassports.toString()
    }
}