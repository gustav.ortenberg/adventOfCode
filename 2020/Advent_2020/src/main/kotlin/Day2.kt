object Day2 : Day {
    data class Rule(
        val min: Int,
        val max: Int,
        val letter: Char
    )

    fun parseRow(row: String): Pair<String, Rule> {
        val password = row.split(' ').last()
        val min = row.split(' ').first().split('-').first().toInt()
        val max = row.split(' ').first().split('-').last().toInt()
        val letter = row.split(':').first().split(' ').last().toCharArray().first()
        return Pair(password, Rule(min, max, letter))
    }

    fun okayPassword(password: String, rule: Rule): Boolean {
        val numberOfOccurrences = password.toList().count { it == rule.letter }
        if (numberOfOccurrences > rule.max)
            return false
        if (numberOfOccurrences < rule.min)
            return false

        return true
    }

    fun tobogganOkayPassword(password: String, rule: Rule): Boolean {
        return (password.toList()[rule.min - 1] == rule.letter).xor(password.toList()[rule.max - 1] == rule.letter)
    }

    override fun solution1(input: List<String>): String {
        val parsedRows = input.map { parseRow(it) }
        val numberOfCorrectPasswords = parsedRows.map { okayPassword(it.first, it.second) }.count { it }
        return numberOfCorrectPasswords.toString()
    }

    override fun solution2(input: List<String>): String {
        val parsedRows = input.map { parseRow(it) }
        val numberOfCorrectPasswords = parsedRows.map { tobogganOkayPassword(it.first, it.second) }.count { it }
        return numberOfCorrectPasswords.toString()
    }

}