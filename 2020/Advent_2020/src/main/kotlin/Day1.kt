object Day1 : Day {
    fun findPairSum(sum: Int, list: List<Int>): Pair<Int, Int> {
        list.forEachIndexed { xIn, x ->
            list.minus(x).forEachIndexed { yIn, y ->
                if (x + y == sum) return Pair(x, y)
            }
        }
        return Pair(0, 0)
    }

    fun findTripleSum(sum: Int, list: List<Int>): Triple<Int, Int, Int> {
        list.forEachIndexed { xIn, x ->
            list.minus(x).forEachIndexed { yIn, y ->
                list.minus(y).forEachIndexed { zIn, z ->
                    if (x + y + z == sum) return Triple(x, y, z)
                }
            }
        }
        return Triple(0, 0, 0)
    }

    override fun solution1(input: List<String>): String {
        val pair = findPairSum(2020, input.map { it.toInt() })
        println(pair)
        val product = pair.first * pair.second
        return product.toString()
    }

    override fun solution2(input: List<String>): String {
        val triple = findTripleSum(2020, input.map{ it.toInt() })
        println(triple)
        val product = triple.toList().reduce { acc, x -> acc * x }
        return product.toString()
    }
}