object Day3 : Day {

    data class Slope(
        val right: Int,
        val down: Int
    )

    data class TreeMap(
        val rows: List<List<Char>>,
        val rowLength: Int
    )

    fun isTree(location: Char): Boolean {
        return location == '#'
    }

    fun encounteredTrees(slope: Slope, map: TreeMap): Long {
        var currentRow = slope.down
        var currentIndentation = slope.right
        var encounteredTrees = 0
        while (currentRow < map.rows.count()) {
            //println("$currentRow : ${currentIndentation%map.rowLength} is ${map.rows[currentRow][currentIndentation % map.rowLength]}")
            if (isTree(map.rows[currentRow][currentIndentation % map.rowLength])) {
                encounteredTrees++
                //println("Success at: $currentRow : ${currentIndentation%map.rowLength}")
            }
            currentRow += slope.down
            currentIndentation += slope.right
        }
        return encounteredTrees.toLong();
    }

    override fun solution1(input: List<String>): String {
        val parsedList = input.map { it.toList() }
        val numberOfEncounteredTrees = encounteredTrees(
            Slope(3, 1),
            TreeMap(parsedList, parsedList.first().count())
        )
        return numberOfEncounteredTrees.toString()
    }

    override fun solution2(input: List<String>): String {
        val parsedList = input.map { it.toList() }
        val map = TreeMap(parsedList, parsedList.first().count())
        val slopes = listOf(
            Slope(1, 1),
            Slope(3, 1),
            Slope(5, 1),
            Slope(7, 1),
            Slope(1, 2)
        )
        val treesOnSlopes = slopes.map { encounteredTrees(it, map) }
        val productOfTrees = treesOnSlopes.reduce { acc, i -> acc * i }
        return productOfTrees.toString()
    }
}